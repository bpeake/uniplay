﻿using UniPlay.Gameplay;
using UnityEngine;

public class DefaultPaddleInput
    : PaddleInput
{
    PlayerPaddleBinding m_binding;

    private PlayerContext GetPlayer()
    {
        return GameContext.GetInstance().GetPlayerContext(m_binding);
    }

    public override int GetCallOrder()
    {
        return -1;
    }

    protected override void OnSetup()
    {
        m_binding = new PlayerPaddleBinding(Owner);
    }

    protected override void OnBeginPlay()
    {
        Listen<IFixedTickEventListener>();
    }

    protected override void OnFixedTick()
    {
        base.OnFixedTick();

        PlayerContext playerContext = GetPlayer();
        if (playerContext != null)
        {
            int playerIndex = playerContext.Player.GetBinding<PlayerIndexBinding>().Index;

            float x = Input.GetAxisRaw(string.Format("Horizontal{0}", playerIndex));
            float y = Input.GetAxisRaw(string.Format("Vertical{0}", playerIndex));

            Owner.MovementBehaviour.SetInput(new Vector2(x, y));
        }
    }

    protected override void OnEndPlay()
    {
        Mute<IFixedTickEventListener>();
    }

    protected override void OnCleanup()
    {
    }
}