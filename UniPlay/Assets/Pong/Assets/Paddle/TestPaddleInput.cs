﻿using UniPlay.Gameplay;
using UnityEngine;

public class TestPaddleInput
    : PaddleInput
{
    public override int GetCallOrder()
    {
        return -1;
    }

    protected override void OnSetup()
    {
    }

    protected override void OnBeginPlay()
    {
        Listen<IFixedTickEventListener>();
    }

    protected override void OnFixedTick()
    {
        base.OnFixedTick();

        float x = Input.GetAxisRaw("Horizontal");
        float y = Input.GetAxisRaw("Vertical");

        Owner.MovementBehaviour.SetInput(new Vector2(x, y));
    }

    protected override void OnEndPlay()
    {
        Mute<IFixedTickEventListener>();
    }

    protected override void OnCleanup()
    {
    }
}