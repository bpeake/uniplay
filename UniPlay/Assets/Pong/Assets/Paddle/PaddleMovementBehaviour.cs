﻿using UnityEngine;
using System.Collections;
using UniPlay.Behaviors;
using UniPlay.Gameplay;

public abstract class PaddleMovementBehaviour
    : SubBehavior<PaddleContext>
{
    private Vector2 m_inputVector;

    protected Vector2 InputVector
    {
        get { return m_inputVector; }
    }

    protected Vector2 EatInput()
    {
        Vector2 input = m_inputVector;
        m_inputVector = Vector2.zero;

        return input;
    }

    public void SetInput(Vector2 input)
    {
        m_inputVector = input;
    }
}