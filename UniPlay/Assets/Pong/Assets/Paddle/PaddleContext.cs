﻿using UnityEngine;
using System.Collections;
using UniPlay.Behaviors;
using UniPlay.Gameplay;
using UniPlay.Messages;

public struct HitPaddleMessageArgs
{
    private PaddleContext m_paddle;
    private Collision2D m_collisionData;

    public PaddleContext Paddle
    {
        get { return m_paddle; }
    }

    public Collision2D CollisionData
    {
        get { return m_collisionData; }
    }

    public HitPaddleMessageArgs(PaddleContext paddle, Collision2D collisionData) 
        : this()
    {
        m_collisionData = collisionData;
        m_paddle = paddle;
    }
}

public interface IHitPaddleMessageReceiver
    : IMessageReceiver
{
    void OnHitPaddle(ref HitPaddleMessageArgs args);
}

public class PaddleContext
    : ObjectContext,
    ICollision2DEnterMessageReceiver
{
    [Part, SerializeField]
    private Rigidbody2D m_rigidbody2D;

    [Part("Collision"), SerializeField]
    private BoxCollider2D m_collision;

    [Part("Visuals"), SerializeField]
    private Transform m_visuals;

    [Binding("Movement"), SerializeField]
    private PaddleMovementBehaviour m_movementBehaviour;

    [Binding("Input"), SerializeField]
    private PaddleInput m_input;

    [SerializeField]
    private float m_speed = 500.0f;

    public Rigidbody2D Physics
    {
        get { return m_rigidbody2D; }
    }

    public PaddleMovementBehaviour MovementBehaviour
    {
        get { return m_movementBehaviour; }
    }

    public float Speed
    {
        get { return m_speed; }
    }

    protected override void OnConstruct()
    {
        base.OnConstruct();

        m_rigidbody2D.gravityScale = 0.0f;
        m_rigidbody2D.constraints = RigidbodyConstraints2D.FreezeRotation;
        m_rigidbody2D.collisionDetectionMode = CollisionDetectionMode2D.Continuous;
    }

    protected override void OnSetup()
    {
    }

    protected override void OnBeginPlay()
    {
        Listen<ITickEventListener>();
        Listen<IFixedTickEventListener>();
    }

    protected override void OnEndPlay()
    {
        Mute<ITickEventListener>();
        Mute<IFixedTickEventListener>();
    }

    protected override void OnCleanup()
    {
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        HitPaddleMessageArgs args = new HitPaddleMessageArgs(this, collision);
        MessageDispatcher.SendMessage<IHitPaddleMessageReceiver>(collision.gameObject, handler =>
        {
            handler.OnHitPaddle(ref args);
        });
    }
}
