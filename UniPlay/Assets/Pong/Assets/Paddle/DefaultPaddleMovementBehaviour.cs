﻿using UniPlay.Gameplay;
using UnityEngine;

public class DefaultPaddleMovementBehaviour
    : PaddleMovementBehaviour
{
    [SerializeField]
    private float m_speedModifier = 1.0f;

    protected override void OnSetup()
    {
        
    }

    protected override void OnBeginPlay()
    {
        Owner.Physics.constraints |= RigidbodyConstraints2D.FreezePositionX;

        Listen<IFixedTickEventListener>();
    }

    protected override void OnFixedTick()
    {
        Vector2 input = Vector2.ClampMagnitude(EatInput(), 1.0f);
        input.x = 0;

        Owner.Physics.velocity = input * (Owner.Speed * m_speedModifier);
    }

    protected override void OnEndPlay()
    {
        Mute<IFixedTickEventListener>();

        Owner.Physics.constraints |= ~RigidbodyConstraints2D.FreezePositionX;
    }

    protected override void OnCleanup()
    {
    }
}