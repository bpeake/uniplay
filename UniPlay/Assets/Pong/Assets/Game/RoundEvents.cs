﻿using UniPlay.Events;

public struct RoundStartEventArgs
{
    private int m_roundNumber;

    public int RoundNumber
    {
        get { return m_roundNumber; }
    }

    public RoundStartEventArgs(int roundNumber) : this()
    {
        m_roundNumber = roundNumber;
    }
}

public struct RoundEndEventArgs
{
}

public interface IRoundEventListener
    : IEventListener<RoundEventDispatacher>
{
    void OnRoundStartEvent(ref RoundStartEventArgs args);
    void OnRoundEndEvent(ref RoundEndEventArgs args);
}

public class RoundEventDispatacher
    : EventDispatcher
{
    public void OnRoundStart(ref RoundStartEventArgs args)
    {
        foreach (IRoundEventListener listener in IterateListeners<IRoundEventListener>())
        {
            listener.OnRoundStartEvent(ref args);
        }
    }

    public void OnRoundEnd(ref RoundEndEventArgs args)
    {
        foreach (IRoundEventListener listener in IterateListeners<IRoundEventListener>())
        {
            listener.OnRoundEndEvent(ref args);
        }
    }
}