﻿using UnityEngine;
using System.Collections;
using UniPlay.Behaviors;

public abstract class PlayerScoreCounter
    : SubBehavior<PongPlayerContext>
{
    public abstract int Score { get; }
}