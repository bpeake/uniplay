﻿
using System.Collections;
using UniPlay.Behaviors;
using UniPlay.Events;
using UnityEngine;

public class PongRuleContinueGameOnRoundEnd
    : SubBehavior<PongGameContext>,
    IRoundEventListener
{
    [SerializeField]
    private float m_timeTillNextRound = 2.0f;

    [SerializeField]
    private int m_endGameScore = 3;

    private int m_roundCount;

    private IEnumerator WaitForTimer()
    {
        yield return new WaitForSeconds(m_timeTillNextRound);

        RoundStartEventArgs roundStartArgs = new RoundStartEventArgs(m_roundCount + 1);
        Owner.StartRound(ref roundStartArgs);
    }

    protected override void OnSetup()
    {
    }

    protected override void OnBeginPlay()
    {
        Listen<IRoundEventListener>();
    }

    protected override void OnEndPlay()
    {
        Mute<IRoundEventListener>();
    }

    protected override void OnCleanup()
    {
    }

    RoundEventDispatacher IEventListener<RoundEventDispatacher>.GetDispatcher()
    {
        return Owner.RoundEvents;
    }

    public void OnRoundStartEvent(ref RoundStartEventArgs args)
    {
        m_roundCount = args.RoundNumber;
    }

    public void OnRoundEndEvent(ref RoundEndEventArgs args)
    {
        foreach (PongPlayerContext playerContext in Owner.IteratePlayingPlayerContexts<PongPlayerContext>())
        {
            if (playerContext.ScoreCounter.Score >= m_endGameScore)
            {
                MatchEndEventArgs matchEndArgs = new MatchEndEventArgs();
                Owner.EndMatch(ref matchEndArgs);
                return;
            }
        }

        StartCoroutine(WaitForTimer());
    }
}