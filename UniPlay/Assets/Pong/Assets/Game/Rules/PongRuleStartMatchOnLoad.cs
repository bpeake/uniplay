﻿
using UniPlay.Behaviors;

public class PongRuleStartMatchOnLoad
    : SubBehavior<PongGameContext>
{
    protected override void OnSetup()
    {
    }

    protected override void OnBeginPlay()
    {
        MatchStartEventArgs args = new MatchStartEventArgs();
        Owner.StartMatch(ref args);
    }

    protected override void OnEndPlay()
    {
    }

    protected override void OnCleanup()
    {
    }
}