﻿
using System;
using UniPlay.Behaviors;
using UniPlay.Events;

public class PongRuleKillBallOnGoal
    : SubBehavior<PongGameContext>,
    IGoalScoredEventListener
{
    protected override void OnSetup()
    {
    }

    protected override void OnBeginPlay()
    {
        Listen<IGoalScoredEventListener>();
    }

    protected override void OnEndPlay()
    {
        Mute<IGoalScoredEventListener>();
    }

    protected override void OnCleanup()
    {
    }

    GoalEventDispatcher IEventListener<GoalEventDispatcher>.GetDispatcher()
    {
        return Owner.GoalEvents;
    }

    public override int GetCallOrder()
    {
        return int.MaxValue;
    }

    public void OnGoalScoredEvent(ref GoalScoredEventArgs args)
    {
        args.Ball.EndPlay();
    }
}