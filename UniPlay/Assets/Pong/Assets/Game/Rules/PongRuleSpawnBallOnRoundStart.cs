﻿
using UniPlay.Behaviors;
using UniPlay.Events;
using UniPlay.Gameplay;
using UnityEngine;

public class PongRuleSpawnBallOnRoundStart
    : SubBehavior<PongGameContext>,
    IRoundEventListener
{
    [SerializeField]
    private BallContext m_ball;

    [Part("SpawnPoint"), SerializeField]
    private Transform m_spawnPoint;

    protected override void OnSetup()
    {
    }

    protected override void OnBeginPlay()
    {
        Listen<IRoundEventListener>();
    }

    protected override void OnEndPlay()
    {
        Mute<IRoundEventListener>();
    }

    protected override void OnCleanup()
    {
    }

    RoundEventDispatacher IEventListener<RoundEventDispatacher>.GetDispatcher()
    {
        return GameContext.GetInstance<PongGameContext>().RoundEvents;
    }

    public void OnRoundStartEvent(ref RoundStartEventArgs args)
    {
        ObjectContext.Spawn(m_ball, Owner, m_spawnPoint.transform.position, m_ball.transform.rotation);
    }

    public void OnRoundEndEvent(ref RoundEndEventArgs args)
    {
    }
}