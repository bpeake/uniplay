﻿
using System.Collections;
using System.Collections.Generic;
using UniPlay.Behaviors;
using UniPlay.Events;
using UnityEngine;

public class PongRuleStartRoundAfterMatchStart
    : SubBehavior<PongGameContext>,
    IMatchEventListener
{
    [SerializeField]
    private float m_delay = 2.0f;

    private IEnumerator m_startTimer;

    private IEnumerator WaitForStartupTimer()
    {
        yield return new WaitForSeconds(m_delay);

        m_startTimer = null;

        RoundStartEventArgs args = new RoundStartEventArgs();
        Owner.StartRound(ref args);
    }

    protected override void OnSetup()
    {
    }

    protected override void OnBeginPlay()
    {
        Listen<IMatchEventListener>();
    }

    protected override void OnEndPlay()
    {
        Mute<IMatchEventListener>();
    }

    protected override void OnCleanup()
    {
    }

    MatchEventDispatacher IEventListener<MatchEventDispatacher>.GetDispatcher()
    {
        return Owner.MatchEvents;
    }

    public void OnMatchStartEvent(ref MatchStartEventArgs args)
    {
        StartCoroutine(m_startTimer = WaitForStartupTimer());
    }

    public void OnMatchEndEvent(ref MatchEndEventArgs args)
    {
        if (m_startTimer != null)
        {
            StopCoroutine(m_startTimer);
        }
    }
}