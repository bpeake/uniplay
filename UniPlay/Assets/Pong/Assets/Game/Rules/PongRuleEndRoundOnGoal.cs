﻿
using UniPlay.Behaviors;
using UniPlay.Events;

public class PongRuleEndRoundOnGoal
    : SubBehavior<PongGameContext>,
    IGoalScoredEventListener
{
    protected override void OnSetup()
    {
    }

    protected override void OnBeginPlay()
    {
        Listen<IGoalScoredEventListener>();
    }

    protected override void OnEndPlay()
    {
        Listen<IGoalScoredEventListener>();
    }

    protected override void OnCleanup()
    {
    }

    public override int GetCallOrder()
    {
        return int.MaxValue;
    }

    GoalEventDispatcher IEventListener<GoalEventDispatcher>.GetDispatcher()
    {
        return Owner.GoalEvents;
    }

    public void OnGoalScoredEvent(ref GoalScoredEventArgs args)
    {
        RoundEndEventArgs roundEndArgs = new RoundEndEventArgs();
        Owner.EndRound(ref roundEndArgs);
    }
}