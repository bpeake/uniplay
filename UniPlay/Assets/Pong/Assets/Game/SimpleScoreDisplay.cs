﻿

using UniPlay.Behaviors;
using UniPlay.Events;
using UniPlay.Gameplay;
using UnityEngine;
using UnityEngine.Assertions;

public class SimpleScoreDisplay
    : SubBehavior<ObjectContext>,
        IGoalScoredEventListener
{
    [SerializeField]
    private PaddleContext m_playerPaddle;

    [SerializeField]
    private Sprite[] m_digits = new Sprite[10];

    [Part("Display"), SerializeField]
    private SpriteRenderer m_display;

    private PlayerPaddleBinding m_playerBinding;

    private void UpdateDisplay()
    {
        Assert.IsTrue(m_digits.Length > 0);

        PongPlayerContext playerContext =
            GameContext.GetInstance().GetPlayerContext<PongPlayerContext>(m_playerBinding);

        if (playerContext != null)
        {
            int index = playerContext.ScoreCounter.Score;
            if (index >= 0 && index < m_digits.Length)
            {
                m_display.sprite = m_digits[index];
            }
        }
        else
        {
            m_display.sprite = null;
        }
    }

    protected override void OnSetup()
    {
        m_playerBinding = new PlayerPaddleBinding(m_playerPaddle);
    }

    protected override void OnBeginPlay()
    {
        Listen<IGoalScoredEventListener>();

        UpdateDisplay();
    }

    protected override void OnEndPlay()
    {
        Mute<IGoalScoredEventListener>();
    }

    protected override void OnCleanup()
    {
    }

    GoalEventDispatcher IEventListener<GoalEventDispatcher>.GetDispatcher()
    {
        return GameContext.GetInstance<PongGameContext>().GoalEvents;
    }

    public override int GetCallOrder()
    {
        return int.MaxValue;
    }

    public void OnGoalScoredEvent(ref GoalScoredEventArgs args)
    {
        UpdateDisplay();
    }
}