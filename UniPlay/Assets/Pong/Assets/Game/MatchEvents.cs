﻿using UniPlay.Events;

public struct MatchStartEventArgs
{
}

public struct MatchEndEventArgs
{
}

public interface IMatchEventListener
    : IEventListener<MatchEventDispatacher>
{
    void OnMatchStartEvent(ref MatchStartEventArgs args);
    void OnMatchEndEvent(ref MatchEndEventArgs args);
}

public class MatchEventDispatacher
    : EventDispatcher
{
    public void OnMatchStart(ref MatchStartEventArgs args)
    {
        foreach (IMatchEventListener listener in IterateListeners<IMatchEventListener>())
        {
            listener.OnMatchStartEvent(ref args);
        }
    }

    public void OnMatchEnd(ref MatchEndEventArgs args)
    {
        foreach (IMatchEventListener listener in IterateListeners<IMatchEventListener>())
        {
            listener.OnMatchEndEvent(ref args);
        }
    }
}