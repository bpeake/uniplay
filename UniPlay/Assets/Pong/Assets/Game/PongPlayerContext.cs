﻿using UnityEngine;
using System.Collections;
using UniPlay.Behaviors;
using UniPlay.Gameplay;

public class PongPlayerContext 
    : PlayerContext
{
    private PaddleContext m_paddle;

    [Binding, SerializeField]
    private PlayerScoreCounter m_scoreCounter;

    public PaddleContext Paddle
    {
        get { return m_paddle; }
    }

    public PlayerScoreCounter ScoreCounter
    {
        get { return m_scoreCounter; }
    }

    protected override void OnSetup()
    {
    }

    protected override void OnBeginPlay()
    {
        m_paddle = Player.GetBinding<PlayerPaddleBinding>().Paddle;
    }

    protected override void OnEndPlay()
    {
        m_paddle = null;
    }

    protected override void OnCleanup()
    {
    }
}
