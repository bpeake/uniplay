﻿using UniPlay.Gameplay;
using UniPlay.Player;
using UnityEngine;

public class TwoPlayerPongGameContext 
    : PongGameContext
{
    [SerializeField]
    private PaddleContext m_leftPaddle;

    [SerializeField]
    private PaddleContext m_rightPaddle;

    public override void StartMatch(ref MatchStartEventArgs args)
    {
        PlayerInfo player1 = PlayerManager.GetInstance().Store.CreatePlayer(new PlayerIndexBinding(0));
        player1.AddBinding(new PlayerPaddleBinding(m_leftPaddle));
        player1.IsPlaying = true;

        PlayerInfo player2 = PlayerManager.GetInstance().Store.CreatePlayer(new PlayerIndexBinding(1));
        player2.AddBinding(new PlayerPaddleBinding(m_rightPaddle));
        player2.IsPlaying = true;

        base.StartMatch(ref args);
    }

    public override void EndMatch(ref MatchEndEventArgs args)
    {
        PlayerManager.Reset();

        base.EndMatch(ref args);
    }
}