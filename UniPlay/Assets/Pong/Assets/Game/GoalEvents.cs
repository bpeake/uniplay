﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UniPlay.Events;

public struct GoalScoredEventArgs
{
    private GoalTag m_goal;
    private BallContext m_ball;
    private int m_pointValue;

    public GoalTag Goal
    {
        get { return m_goal; }
    }

    public BallContext Ball
    {
        get { return m_ball; }
    }

    public int PointValue
    {
        get { return m_pointValue; }
    }

    public GoalScoredEventArgs(GoalTag goal, BallContext ball, int pointValue = 1) : this()
    {
        m_goal = goal;
        m_ball = ball;
        m_pointValue = pointValue;
    }
}

public interface IGoalScoredEventListener
    : IEventListener<GoalEventDispatcher>
{
    void OnGoalScoredEvent(ref GoalScoredEventArgs args);
}

public class GoalEventDispatcher
    : EventDispatcher
{
    public void OnGoalScored(ref GoalScoredEventArgs args)
    {
        foreach (IGoalScoredEventListener listener in IterateListeners<IGoalScoredEventListener>())
        {
            listener.OnGoalScoredEvent(ref args);
        }
    }
}