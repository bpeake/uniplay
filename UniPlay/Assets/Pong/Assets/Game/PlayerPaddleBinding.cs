﻿using UnityEngine;
using System.Collections;
using UniPlay.Player;

public class PlayerPaddleBinding
    : IPlayerBinding
{
    public static bool operator ==(PlayerPaddleBinding left, PlayerPaddleBinding right)
    {
        return Equals(left, right);
    }

    public static bool operator !=(PlayerPaddleBinding left, PlayerPaddleBinding right)
    {
        return !Equals(left, right);
    }

    private readonly PaddleContext m_paddle;

    public PaddleContext Paddle
    {
        get { return m_paddle; }
    }

    public PlayerPaddleBinding(PaddleContext paddle)
    {
        m_paddle = paddle;
    }

    public int GetOrder()
    {
        return (int) (m_paddle.gameObject.transform.position.x * 1000f);
    }

    protected bool Equals(PlayerPaddleBinding other)
    {
        return m_paddle == other.m_paddle;
    }

    public override bool Equals(object obj)
    {
        if (ReferenceEquals(null, obj)) return false;
        if (ReferenceEquals(this, obj)) return true;
        if (obj.GetType() != this.GetType()) return false;
        return Equals((PlayerPaddleBinding) obj);
    }

    public override int GetHashCode()
    {
        return m_paddle.GetHashCode();
    }
}
