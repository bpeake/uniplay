﻿using UnityEngine;
using System.Collections;
using UniPlay.Player;

public class PlayerIndexBinding
    : IPlayerBinding
{
    public static bool operator ==(PlayerIndexBinding left, PlayerIndexBinding right)
    {
        return Equals(left, right);
    }

    public static bool operator !=(PlayerIndexBinding left, PlayerIndexBinding right)
    {
        return !Equals(left, right);
    }

    private readonly int m_index;

    public int Index
    {
        get { return m_index; }
    }

    public PlayerIndexBinding(int index)
    {
        m_index = index;
    }

    public int GetOrder()
    {
        return m_index;
    }

    protected bool Equals(PlayerIndexBinding other)
    {
        return m_index == other.m_index;
    }

    public override bool Equals(object obj)
    {
        if (ReferenceEquals(null, obj)) return false;
        if (ReferenceEquals(this, obj)) return true;
        if (obj.GetType() != this.GetType()) return false;
        return Equals((PlayerIndexBinding) obj);
    }

    public override int GetHashCode()
    {
        return m_index;
    }
}
