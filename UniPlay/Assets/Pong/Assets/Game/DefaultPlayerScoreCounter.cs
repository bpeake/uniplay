using UniPlay.Events;
using UniPlay.Gameplay;

public class DefaultPlayerScoreCounter
    : PlayerScoreCounter,
    IGoalScoredEventListener
{
    private int m_score;

    public override int Score
    {
        get { return m_score; }
    }

    protected override void OnSetup()
    {
    }

    protected override void OnBeginPlay()
    {
        Listen<IGoalScoredEventListener>();
    }

    protected override void OnEndPlay()
    {
        Mute<IGoalScoredEventListener>();
    }

    protected override void OnCleanup()
    {
    }

    GoalEventDispatcher IEventListener<GoalEventDispatcher>.GetDispatcher()
    {
        return GameContext.GetInstance<PongGameContext>().GoalEvents;
    }

    public void OnGoalScoredEvent(ref GoalScoredEventArgs args)
    {
        if(args.Goal.Paddle != Owner.Paddle)
        {
            m_score += args.PointValue;
        }
    }
}