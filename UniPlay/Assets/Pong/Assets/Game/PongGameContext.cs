﻿using UnityEngine;
using System.Collections;
using UniPlay.Gameplay;

public abstract class PongGameContext 
    : GameContext
{
    private RoundEventDispatacher m_roundEvents = new RoundEventDispatacher();
    private MatchEventDispatacher m_matchEvents = new MatchEventDispatacher();
    private GoalEventDispatcher m_goalEvents = new GoalEventDispatcher();

    [SerializeField]
    private PongPlayerContext m_defaultReadyPlayerPrefab;

    [SerializeField]
    private PlayerContext m_defaultNotReadyPlayerPrefab;

    public RoundEventDispatacher RoundEvents
    {
        get { return m_roundEvents; }
    }

    public MatchEventDispatacher MatchEvents
    {
        get { return m_matchEvents; }
    }

    public GoalEventDispatcher GoalEvents
    {
        get { return m_goalEvents; }
    }

    public override PlayerContext DefaultReadyPlayerPrefab
    {
        get { return m_defaultReadyPlayerPrefab; }
    }

    public override PlayerContext DefaultNotReadyPlayerPrefab
    {
        get { return m_defaultNotReadyPlayerPrefab; }
    }

    protected override void OnSetup()
    {
    }

    protected override void OnCleanup()
    {
    }

    public virtual void StartMatch(ref MatchStartEventArgs args)
    {
        m_matchEvents.OnMatchStart(ref args);
    }

    public virtual void EndMatch(ref MatchEndEventArgs args)
    {
        m_matchEvents.OnMatchEnd(ref args);
    }

    public virtual void StartRound(ref RoundStartEventArgs args)
    {
        m_roundEvents.OnRoundStart(ref args);
    }

    public virtual void EndRound(ref RoundEndEventArgs args)
    {
        m_roundEvents.OnRoundEnd(ref args);
    }

    public virtual void ScoreGoal(ref GoalScoredEventArgs args)
    {
        m_goalEvents.OnGoalScored(ref args);
    }
}