﻿
using UniPlay.Behaviors;
using UniPlay.Messages;
using UnityEngine;

public struct HitWallMessageArgs
{
    private WallTag m_wall;
    private Collision2D m_collisionData;

    public HitWallMessageArgs(WallTag wall, Collision2D collisionData) : this()
    {
        m_wall = wall;
        m_collisionData = collisionData;
    }

    public WallTag Wall
    {
        get { return m_wall; }
    }

    public Collision2D CollisionData
    {
        get { return m_collisionData; }
    }
}

public interface IHitWallMessageReceiver
    : IMessageReceiver
{
    void OnHitWall(ref HitWallMessageArgs args);
}

public class WallTag
    : SubBehavior<ObjectContext>,
    ICollision2DEnterMessageReceiver
{
    protected override void OnSetup()
    {
    }

    protected override void OnBeginPlay()
    {
    }

    protected override void OnEndPlay()
    {
    }

    protected override void OnCleanup()
    {
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        HitWallMessageArgs args = new HitWallMessageArgs(this, collision);
        MessageDispatcher.SendMessage<IHitWallMessageReceiver>(collision.gameObject, handler =>
        {
            handler.OnHitWall(ref args);
        });
    }
}