﻿
using UniPlay.Behaviors;
using UniPlay.Gameplay;
using UnityEngine;

public class GoalTag
    : SubBehavior<ObjectContext>,
    ITriggerBallMessageReceiver
{
    [SerializeField]
    private PaddleContext m_paddle;

    public PaddleContext Paddle
    {
        get { return m_paddle; }
    }

    protected override void OnSetup()
    {
    }

    protected override void OnBeginPlay()
    {
    }

    protected override void OnEndPlay()
    {
    }

    protected override void OnCleanup()
    {
    }

    public void OnTriggerBall(ref TriggerBallMessageArgs args)
    {
        GoalScoredEventArgs goalArgs = new GoalScoredEventArgs(this, args.Ball);
        GameContext.GetInstance<PongGameContext>().ScoreGoal(ref goalArgs);
    }
}