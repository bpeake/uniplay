﻿using UnityEngine;

public class DefaultBallMovementBehaviour
    : BallMovementBehaviour,
    IHitPaddleMessageReceiver,
    IHitWallMessageReceiver
{
    [SerializeField]
    private float m_speedModifier = 1.0f;

    private float GetSpeed()
    {
        return m_speedModifier * Owner.Speed;
    }

    private void Bounce(Collision2D collisionData)
    {
        Vector2 reflectedVelocity = Vector2.Reflect(-collisionData.relativeVelocity, collisionData.contacts[0].normal);
        Owner.Physics.velocity = reflectedVelocity.normalized * GetSpeed();
    }

    protected override void OnSetup()
    {
    }

    protected override void OnBeginPlay()
    {
        float x = Random.Range(-1, 1f);
        if(x > -0.5f && x < 0f)
        {
            x = Random.Range(-1f, -0.5f);
        }
        else if (x < 0.5f && x > 0)
        {
            x = Random.Range(0.5f, 1f);
        }

        float y = Random.Range(-1f, 1f);

        Vector2 dir = new Vector2(x, y).normalized;
        Vector2 velocity = GetSpeed() * dir;

        Owner.Physics.velocity = velocity;
    }

    protected override void OnEndPlay()
    {
    }

    protected override void OnCleanup()
    {
    }

    public void OnHitPaddle(ref HitPaddleMessageArgs args)
    {
        Bounce(args.CollisionData);
    }

    public void OnHitWall(ref HitWallMessageArgs args)
    {
        Bounce(args.CollisionData);
    }
}