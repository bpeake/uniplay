﻿using UnityEngine;
using System.Collections;
using UniPlay.Behaviors;
using UniPlay.Messages;

public struct HitBallMessageArgs
{
    private BallContext m_ball;
    private Collision2D m_collisionData;

    public BallContext Ball
    {
        get { return m_ball; }
    }

    public Collision2D CollisionData
    {
        get { return m_collisionData; }
    }

    public HitBallMessageArgs(BallContext ball, Collision2D collisionData) : this()
    {
        m_ball = ball;
        m_collisionData = collisionData;
    }
}

public interface IHitBallMessageReceiver
    : IMessageReceiver
{
    void OnHitBall(ref HitBallMessageArgs args);
}

public struct TriggerBallMessageArgs
{
    private BallContext m_ball;

    public BallContext Ball
    {
        get { return m_ball; }
    }

    public TriggerBallMessageArgs(BallContext ball) : this()
    {
        m_ball = ball;
    }
}

public interface ITriggerBallMessageReceiver
    : IMessageReceiver
{
    void OnTriggerBall(ref TriggerBallMessageArgs args);
}

public class BallContext 
    : ObjectContext,
    ICollision2DEnterMessageReceiver,
    ITriggerEnter2DMessageReceiver
{
    [SerializeField]
    private float m_speed = 100.0f;

    [Part("Visuals"), SerializeField]
    private Transform m_visuals;

    [Part("Collision"), SerializeField]
    private CircleCollider2D m_collision;

    [Part, SerializeField]
    private Rigidbody2D m_rigidbody2D;

    [Binding("Movement"), SerializeField]
    private BallMovementBehaviour m_movementBehaviour;

    public float Speed
    {
        get { return m_speed; }
    }

    public Rigidbody2D Physics
    {
        get { return m_rigidbody2D; }
    }

    protected override void OnConstruct()
    {
        base.OnConstruct();

        m_rigidbody2D.constraints = RigidbodyConstraints2D.FreezeRotation;
        m_rigidbody2D.gravityScale = 0.0f;
        m_rigidbody2D.collisionDetectionMode = CollisionDetectionMode2D.Continuous;
        m_rigidbody2D.mass = float.Epsilon;
    }

    protected override void OnSetup()
    {
    }

    protected override void OnBeginPlay()
    {
    }

    protected override void OnEndPlay()
    {
    }

    protected override void OnCleanup()
    {
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        HitBallMessageArgs args = new HitBallMessageArgs(this, collision);
        MessageDispatcher.SendMessage<IHitBallMessageReceiver>(collision.gameObject, handler =>
        {
            handler.OnHitBall(ref args);
        });
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        TriggerBallMessageArgs args = new TriggerBallMessageArgs(this);
        MessageDispatcher.SendMessage<ITriggerBallMessageReceiver>(other.gameObject, handler =>
        {
            handler.OnTriggerBall(ref args);
        });
    }
}
