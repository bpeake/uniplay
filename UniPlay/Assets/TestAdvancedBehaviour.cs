﻿using UniPlay.Behaviors;
using UnityEngine;

namespace Assets
{
    public class TestAdvancedBehaviour : ObjectContext
    {
        [Part("Collision/Sphere"), SerializeField]
        private SphereCollider m_collider;

        [Binding, SerializeField]
        private Collider m_colliderBinding;

        protected override void OnSetup()
        {
        
        }

        protected override void OnBeginPlay()
        {
        
        }

        protected override void OnEndPlay()
        {
        
        }

        protected override void OnCleanup()
        {
        
        }
    }
}
