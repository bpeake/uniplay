﻿using UnityEngine;
using System.Collections;
using UniPlay.Behaviors;
using UniPlay.Gameplay;

public class TestTicker : ObjectContext
{
    protected override void OnSetup()
    {
    }

    protected override void OnBeginPlay()
    {
        Listen<ITickEventListener, TickEventDispatcher>();
    }

    protected override void OnTick()
    {
        Debug.Log("Tick");
    }

    protected override void OnEndPlay()
    {
    }

    protected override void OnCleanup()
    {
    }
}
