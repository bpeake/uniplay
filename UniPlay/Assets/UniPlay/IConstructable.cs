﻿using System.Collections;
using UnityEngine;

namespace UniPlay
{
    public interface IConstructable<T>
    {
        void Construct(T param);
    }

    public interface IOptionalConstructable<T> : IConstructable<T>
    {
    }
}