﻿
using UniPlay.Gameplay;
using UnityEngine;

namespace UniPlay.Behaviors
{
    public abstract class SubBehavior<T>
        : AdvancedBehavior
        where T : ObjectContext
    {
        private T m_owner;

        /// <summary>
        /// The Bridge of this sub behaviour.
        /// </summary>
        public T Owner
        {
            get
            {
                if(!m_owner)
                {
                    T[] parents = GetComponentsInParent<T>(true);
                    if (parents.Length > 0)
                    {
                        m_owner = parents[0];
                    }
                    else
                    {
                        m_owner = null;
                    }
                }

                return m_owner;
            }
        }

        public override Transform Root
        {
            get
            {
                T owner = Owner;
                if (owner)
                {
                    return owner.transform;
                }
                else
                {
                    return null;
                }
            }
        }

        protected override PlayEventDispatcher GetPlayEventDispatcher()
        {
            T owner = Owner;
            if (owner)
            {
                return owner.PostPlayEventDispatcher;
            }
            else
            {
                return null;
            }
        }

        protected override TickEventDispatcher GetTickEventDispatcher()
        {
            T owner = Owner;
            if (owner)
            {
                return owner.PostTickEventDispatcher;
            }
            else
            {
                return null;
            }
        }

        protected override LateTickEventDispatcher GetLateTickEventDispatcher()
        {
            T owner = Owner;
            if (owner)
            {
                return owner.PostLateTickEventDispatcher;
            }
            else
            {
                return null;
            }
        }

        protected override FixedTickEventDispatcher GetFixedTickEventDispatcher()
        {
            T owner = Owner;
            if (owner)
            {
                return owner.PostFixedTickEventDispatcher;
            }
            else
            {
                return null;
            }
        }

        public override int GetCallOrder()
        {
            return 0;
        }
    }
}