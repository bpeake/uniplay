﻿namespace UniPlay.Behaviors
{
    public class Slot
        : SubBehavior<ObjectContext>
    {
        public override int GetCallOrder()
        {
            return int.MinValue;
        }

        protected override void OnSetup()
        {
        }

        protected override void OnBeginPlay()
        {
        }

        protected override void OnEndPlay()
        {
        }

        protected override void OnCleanup()
        {
        }
    }
}