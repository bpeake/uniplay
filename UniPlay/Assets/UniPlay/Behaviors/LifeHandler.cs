﻿
using UniPlay.Behaviors;

public abstract class LifeHandler
    : SubBehavior<ObjectContext>
{
    protected override void OnSetup()
    {
    }

    protected override void OnBeginPlay()
    {
    }

    protected override void OnCleanup()
    {
    }
}