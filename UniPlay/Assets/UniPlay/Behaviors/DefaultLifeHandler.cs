﻿
using UnityEngine;

public class DefaultLifeHandler
    : LifeHandler
{
    protected override void OnSetup()
    {
        base.OnSetup();

        hideFlags = HideFlags.HideInInspector;
    }

    protected override void OnEndPlay()
    {
        Destroy(gameObject);
    }
}