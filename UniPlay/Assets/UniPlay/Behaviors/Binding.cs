﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UniPlay.Reflection;
using UnityEngine;
using UnityEngine.Assertions;

namespace UniPlay.Behaviors
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public sealed class BindingAttribute
        : PropertyAttribute
    {
        private string m_offset;

        public string Offset
        {
            get { return m_offset; }
        }

        public string Slot { get; set; }

        public BindingAttribute()
        {
            m_offset = "";
        }

        public BindingAttribute(string offset)
        {
            m_offset = offset;
        }
    }

    public sealed class BindingInfo
    {
        private FieldInfo m_field;
        private string m_fullName;
        private string m_slot;

        public FieldInfo Field
        {
            get { return m_field; }
        }

        public string FullName
        {
            get { return m_fullName; }
        }

        public string Slot
        {
            get { return m_slot; }
        }

        public BindingInfo(FieldInfo field)
        {
            Assert.IsTrue(field.IsDefined(typeof(BindingAttribute), true));

            m_field = field;

            BindingAttribute binding = (BindingAttribute) field.GetCustomAttributes(typeof (BindingAttribute), true)[0];
            if (string.IsNullOrEmpty(binding.Offset))
            {
                m_fullName = string.Format("Bind.{0}", m_field.Name);
            }
            else
            {
                m_fullName = string.Format("{0}/Bind.{1}", binding.Offset, field.Name);
            }

            m_slot = binding.Slot;
        }

        public Transform GetBindingContainer(AdvancedBehavior source)
        {
            AdvancedBehavior root;
            if (string.IsNullOrEmpty(m_slot))
            {
                root = source;
            }
            else
            {
                var foundSlot =
                    source.Root
                        .GetComponentsInChildren<Slot>(true)
                        .FirstOrDefault(slot => slot.gameObject.name == m_slot);

                root = foundSlot ? foundSlot : null;
            }

            if (!root)
                return null;

            var parts = m_fullName.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries);

            var container = m_fullName.StartsWith("/") ? root.Root.transform : root.transform;
            foreach (var part in parts)
            {
                var child = container.FindChild(part);
                if (!child)
                {
                    var newChild = new GameObject(part);
                    child = newChild.transform;
                    child.parent = container;
                    child.transform.localPosition = Vector3.zero;
                }

                container = child;
            }

            return container;
        }

        public Component GetBindValue(AdvancedBehavior source)
        {
            Transform container = GetBindingContainer(source);

            if(container)
                return container.GetComponent(m_field.FieldType);

            return null;
        }

        public void UpdateBinding(AdvancedBehavior source)
        {
            Assert.IsTrue(m_field.DeclaringType.IsInstanceOfType(source));

            m_field.SetValue(source, GetBindValue(source));
        }
    }

    public sealed class TypeBindingCollection
    {
        private Type m_type;

        private Dictionary<FieldInfo, BindingInfo> m_bindingLookup = new Dictionary<FieldInfo, BindingInfo>();

        public Type BaseType
        {
            get { return m_type; }
        }

        public TypeBindingCollection(Type type)
        {
            m_type = type;

            foreach (FieldInfo fieldInfo in TypeHelper.GetAllInstanceFieldsWithAttribute(type, typeof(BindingAttribute)))
            {
                m_bindingLookup.Add(fieldInfo, new BindingInfo(fieldInfo));
            }
        }

        public BindingInfo GetBindingInfo(FieldInfo field)
        {
            BindingInfo info;
            if (m_bindingLookup.TryGetValue(field, out info))
            {
                return info;
            }

            return null;
        }

        public IEnumerable<BindingInfo> IterateBindings()
        {
            return m_bindingLookup.Values;
        }
    }

    public sealed class BindingLibrary
    {
        private static BindingLibrary s_globalLibrary = new BindingLibrary();

        public static BindingLibrary Global
        {
            get { return s_globalLibrary; }
        }

        private Dictionary<Type, TypeBindingCollection> m_types = new Dictionary<Type, TypeBindingCollection>();

        public TypeBindingCollection GetTypeBindings(Type t)
        {
            TypeBindingCollection collection;
            if (m_types.TryGetValue(t, out collection))
            {
                return collection;
            }

            TypeBindingCollection newCollection = new TypeBindingCollection(t);
            m_types.Add(t, newCollection);

            return newCollection;
        }
    }
}