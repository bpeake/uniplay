﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UniPlay.Events;
using UniPlay.Gameplay;
using UniPlay.Reflection;
using UnityEditor;
using UnityEngine;
using UnityEngine.Assertions;
#if UNITY_EDITOR

#endif

namespace UniPlay.Behaviors
{
    /// <summary>
    /// A behaviour that offers advanced features.
    /// </summary>
    public abstract class AdvancedBehavior
        : MonoBehaviour,
            IPlayEventListener,
            ITickEventListener, ILateTickEventListener, IFixedTickEventListener
    {
        private bool m_hasStarted;

        [SerializeField, HideInInspector]
        private bool m_hasConstructed;

        [SerializeField, HideInInspector]
        private List<Component> m_oldParts = new List<Component>(); 

        private PlayEventDispatcher m_prePlayEventDispatcher = new PlayEventDispatcher();
        private PlayEventDispatcher m_postPlayEventDispatcher = new PlayEventDispatcher();

        private TickEventDispatcher m_preTickEventDispatcher = new TickEventDispatcher();
        private TickEventDispatcher m_postTickEventDispatcher = new TickEventDispatcher();
        private LateTickEventDispatcher m_preLateTickEventDispatcher = new LateTickEventDispatcher();
        private LateTickEventDispatcher m_postLateTickEventDispatcher = new LateTickEventDispatcher();
        private FixedTickEventDispatcher m_preFixedTickEventDispatcher = new FixedTickEventDispatcher();
        private FixedTickEventDispatcher m_postFixedTickEventDispatcher = new FixedTickEventDispatcher();

        private TypeBindingCollection m_cachedBindingCollection;

        /// <summary>
        /// Checks to see if the behavior has started or not.
        /// </summary>
        public bool HasStarted
        {
            get { return m_hasStarted; }
        }

        public virtual bool Active
        {
            get { return m_hasStarted && enabled; }
            set
            {
                if(m_hasStarted)
                {
                    enabled = value;
                }
                else
                {
                    enabled = false;
                }
            }
        }

        internal bool HasConstructed
        {
            get { return m_hasConstructed; }
        }

        public abstract Transform Root { get; }

        public PlayEventDispatcher PrePlayEventDispatcher
        {
            get { return m_prePlayEventDispatcher; }
        }

        public PlayEventDispatcher PostPlayEventDispatcher
        {
            get { return m_postPlayEventDispatcher; }
        }

        public TickEventDispatcher PreTickEventDispatcher
        {
            get { return m_preTickEventDispatcher; }
        }

        public TickEventDispatcher PostTickEventDispatcher
        {
            get { return m_postTickEventDispatcher; }
        }

        public LateTickEventDispatcher PreLateTickEventDispatcher
        {
            get { return m_preLateTickEventDispatcher; }
        }

        public LateTickEventDispatcher PostLateTickEventDispatcher
        {
            get { return m_postLateTickEventDispatcher; }
        }

        public FixedTickEventDispatcher PreFixedTickEventDispatcher
        {
            get { return m_preFixedTickEventDispatcher; }
        }

        public FixedTickEventDispatcher PostFixedTickEventDispatcher
        {
            get { return m_postFixedTickEventDispatcher; }
        }

        internal Transform FindContainer(string path)
        {
            if (string.IsNullOrEmpty(path))
                return transform;

            string[] parts = path.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries);

            Transform container = path.StartsWith("/") ? Root : transform;
            foreach (string part in parts)
            {
                Transform child = container.FindChild(part);
                if (!child)
                {
                    return null;
                }

                container = child;
            }

            return container;
        }

        internal Transform ConstructContainer(string path)
        {
            if(string.IsNullOrEmpty(path))
                return transform;

            string[] parts = path.Split(new[] {'/'}, StringSplitOptions.RemoveEmptyEntries);

            Transform container = path.StartsWith("/") ? Root : transform;
            foreach (string part in parts)
            {
                Transform child = container.FindChild(part);
                if (!child)
                {
                    GameObject newChild = new GameObject(part);
                    child = newChild.transform;
                    child.parent = container;
                    child.localPosition = Vector3.zero;
                }

                container = child;
            }

            return container;
        }

        private bool ConstructPart(FieldInfo field)
        {
            if (!typeof (Component).IsAssignableFrom(field.FieldType))
            {
                Debug.LogError("Parts must be placed on fields deriving from the Component class.");
                return false;
            }

            if (!field.IsDefined(typeof (SerializeField), true))
            {
                Debug.LogError("Parts need to also be marked with SerializedField.");
                return false;
            }

            Component currentValue = field.GetValue(this) as Component;
            if (!currentValue)
            {
                Part attribute = (Part)field.GetCustomAttributes(typeof (Part), true)[0];
                Transform container = ConstructContainer(attribute.Container);

                if (typeof(Transform).IsAssignableFrom(field.FieldType))
                {
                    currentValue = container;
                }
                else
                {
                    currentValue = container.gameObject.AddComponent(field.FieldType);
                    if (currentValue == null)
                    {
                        currentValue = container.gameObject.GetComponent(field.FieldType);
                    }
                }

                field.SetValue(this, currentValue);
            }

            AdvancedBehavior advCurrentValue = currentValue as AdvancedBehavior;
            if (advCurrentValue)
            {
                advCurrentValue.InternalConstruct();
            }

            return true;
        }

        private void ConstructParts()
        {
            HashSet<Component> newParts = new HashSet<Component>();

            foreach (FieldInfo field in TypeHelper.GetAllInstanceFieldsWithAttribute(GetType(), typeof(Part)))
            {
                if (ConstructPart(field))
                {
                    newParts.Add((Component) field.GetValue(this));
                }
            }

            List<Component> garbage = m_oldParts.Where(part => !newParts.Contains(part)).ToList();

#if UNITY_EDITOR
            EditorApplication.delayCall += () =>
            {
                foreach (Component component in garbage)
                {
                    DestroyImmediate(component);
                }
            };
#else
        foreach (Component component in garbage)
        {
            Destroy(component);
        }
#endif

            m_oldParts = newParts.ToList();
        }

        private void UpdateBindings()
        {
            if (m_cachedBindingCollection == null)
            {
                m_cachedBindingCollection = BindingLibrary.Global.GetTypeBindings(GetType());
            }

            foreach (BindingInfo binding in m_cachedBindingCollection.IterateBindings())
            {
                binding.UpdateBinding(this);
            }
        }

        internal void Reset()
        {
            m_hasConstructed = false;

            foreach (AdvancedBehavior child in GetComponentsInChildren<AdvancedBehavior>(true))
            {
                child.m_hasConstructed = false;
            }

            InternalConstruct();
        }

        protected internal virtual void Awake()
        {
            InternalConstruct();

            Listen<IPlayEventListener, PlayEventDispatcher>();

            OnSetup();
        }

        protected internal virtual void Start()
        {
            PlayEventDispatcher playEventDispatcher = GetPlayEventDispatcher();

            if (playEventDispatcher != null && playEventDispatcher.HasStarted)
            {
                InternalBeginPlay();
            }
            else
            {
                enabled = false;
            }
        }

        protected internal virtual void OnDestroy()
        {
            InternalEndPlay();
            OnCleanup();
        }

        protected internal virtual void OnTransformChildrenChanged()
        {
            if (m_hasConstructed)
            {
                UpdateBindings();
            }
        }

        internal virtual void InternalConstruct()
        {
            if (!m_hasConstructed)
            {
                ConstructParts();

                OnConstruct();
                m_hasConstructed = true;
            }

            UpdateBindings();
        }

        protected internal virtual void InternalBeginPlay()
        {
            if (m_hasStarted)
                return;

            enabled = true;

            m_prePlayEventDispatcher.OnPlayStart();

            m_hasStarted = true;

            try
            {
                OnBeginPlay();
            }
            catch (Exception e)
            {
                Debug.LogException(e);
            }

            m_postPlayEventDispatcher.OnPlayStart();
        }

        protected internal virtual void InternalEndPlay()
        {
            if (!m_hasStarted)
                return;

            if(this != null)
            {
                enabled = false;
            }

            Mute<ITickEventListener, TickEventDispatcher>();
            Mute<ILateTickEventListener, LateTickEventDispatcher>();
            Mute<IFixedTickEventListener, FixedTickEventDispatcher>();

            m_prePlayEventDispatcher.OnPlayEnd();

            m_hasStarted = false;

            try
            {
                OnEndPlay();
            }
            catch (Exception e)
            {
                Debug.LogException(e);
            }

            m_postPlayEventDispatcher.OnPlayEnd();
        }

        protected internal virtual void InternalTick()
        {
            m_preTickEventDispatcher.Tick();

            try
            {
                OnTick();
            }
            catch (Exception e)
            {
                Debug.LogException(e);
            }

            m_postTickEventDispatcher.Tick();
        }

        protected internal virtual void InternalLateTick()
        {
            m_preLateTickEventDispatcher.Tick();

            try
            {
                OnLateTick();
            }
            catch (Exception e)
            {
                Debug.LogException(e);
            }

            m_postLateTickEventDispatcher.Tick();
        }

        protected internal virtual void InternalFixedTick()
        {
            m_preFixedTickEventDispatcher.Tick();

            try
            {
                OnFixedTick();
            }
            catch (Exception e)
            {
                Debug.LogException(e);
            }

            m_postFixedTickEventDispatcher.Tick();
        }

        PlayEventDispatcher IEventListener<PlayEventDispatcher>.GetDispatcher()
        {
            return GetPlayEventDispatcher();
        }

        TickEventDispatcher IEventListener<TickEventDispatcher>.GetDispatcher()
        {
            return GetTickEventDispatcher();
        }

        LateTickEventDispatcher IEventListener<LateTickEventDispatcher>.GetDispatcher()
        {
            return GetLateTickEventDispatcher();
        }

        FixedTickEventDispatcher IEventListener<FixedTickEventDispatcher>.GetDispatcher()
        {
            return GetFixedTickEventDispatcher();
        }

        void IPlayEventListener.OnBeginPlayEvent()
        {
            BeginPlay();
        }

        void IPlayEventListener.OnEndPlayEvent()
        {
            EndPlay();
        }

        void ITickEventListener.OnTickEvent()
        {
            InternalTick();
        }

        void ILateTickEventListener.OnLateTickEvent()
        {
            InternalLateTick();
        }

        void IFixedTickEventListener.OnFixedTickEvent()
        {
            InternalFixedTick();
        }

        private IEnumerator WaitForStart()
        {
            yield return null;

            InternalBeginPlay();
        }

        private IEnumerator WaitForEnd()
        {
            yield return null;

            InternalEndPlay();
        }

        /// <summary>
        /// Tells this behavior to listen to a type of event.
        /// </summary>
        /// <typeparam name="T">The type of event listener to listen with.</typeparam>
        /// <typeparam name="D">The type of dispatcher to listen to.</typeparam>
        public void Listen<T, D>()
            where D : EventDispatcher
            where T : IEventListener<D>
        {
            Assert.IsTrue(this is T);

            IEventListener e = this;
            T t = (T)e;

            EventDispatcher dispatcher = t.GetDispatcher();
            dispatcher.RegisterEventListener(t);
        }

        /// <summary>
        /// Tells the behaviour to listen to a type of event.
        /// </summary>
        /// <typeparam name="T">The type of listener to listen with.</typeparam>
        /// <remarks>Functionally this is the same to the more verbose version of listen, it just is slightly slower as it uses reflection.</remarks>
        public void Listen<T>()
            where T : IEventListener
        {
            Assert.IsTrue(this is T);

            MethodInfo getDispatcherMethodInfo = TypeHelper.GetMethod(typeof(T), "GetDispatcher", Type.EmptyTypes);

            Assert.IsNotNull(getDispatcherMethodInfo, "No method GetDispatcher was found on this interface");

            EventDispatcher dispatcher = getDispatcherMethodInfo.Invoke(this, new object[0]) as EventDispatcher;

            if (dispatcher != null)
            {
                dispatcher.RegisterEventListener(this);
            }
            else
            {
                Debug.LogError("No valid dispatcher found, cannot listen to given type.", this);
            }
        }

        /// <summary>
        /// Tells this behavior to stop listening to a type of event.
        /// </summary>
        /// <typeparam name="T">The type of event listener to stop listening with.</typeparam>
        /// <typeparam name="D">The type of dispatcher to mute from.</typeparam>
        public void Mute<T, D>()
            where D : EventDispatcher
            where T : IEventListener<D>
        {
            Assert.IsTrue(this is T);

            IEventListener e = this;
            T t = (T)e;

            EventDispatcher dispatcher = t.GetDispatcher();
            dispatcher.RemoveListener(t);
        }

        public void Mute<T>()
            where T : IEventListener
        {
            Assert.IsTrue(this is T);

            MethodInfo getDispatcherMethodInfo = TypeHelper.GetMethod(typeof(T), "GetDispatcher", Type.EmptyTypes);

            Assert.IsNotNull(getDispatcherMethodInfo, "No method GetDispatcher was found on this interface");

            EventDispatcher dispatcher = getDispatcherMethodInfo.Invoke(this, new object[0]) as EventDispatcher;

            if (dispatcher != null)
            {
                dispatcher.RemoveListener(this);
            }
            else
            {
                Debug.LogError("No valid dispatcher found, cannot mute to given type.", this);
            }
        }

        /// <summary>
        /// Gets an event dispatcher for a type of listener.
        /// </summary>
        /// <typeparam name="T">The type of listener.</typeparam>
        /// <typeparam name="D">The type of dispatcher to cast to.</typeparam>
        /// <returns>The event dispatcher for the listener of type T as type D.</returns>
        public D GetDispatcher<T, D>()
            where D : EventDispatcher
            where T : IEventListener<D>
        {
            Assert.IsTrue(this is T);

            IEventListener e = this;
            T t = (T)e;

            D d = t.GetDispatcher() as D;

            return d;
        }

        /// <summary>
        /// Checks to see if we are listening with a specific listener.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="D"></typeparam>
        /// <returns></returns>
        public bool IsListening<T, D>()
            where D : EventDispatcher
            where T : IEventListener<D>
        {
            EventDispatcher dispatcher = GetDispatcher<T, D>();

            return dispatcher.HasListener(this);
        }

        /// <summary>
        /// Starts an object if it has not already been started.
        /// <remarks>The object will start in the next frame of execution.</remarks>
        /// </summary>
        public void BeginPlay()
        {
            if (m_hasStarted)
                return;

            GameInstance.GetInstance().StartCoroutine(WaitForStart());
        }

        /// <summary>
        /// Stops an object if it is currently in play.
        /// <remarks>The object will stop in the next frame of execution.</remarks>
        /// </summary>
        public void EndPlay()
        {
            if (!m_hasStarted)
                return;

            GameInstance.GetInstance().StartCoroutine(WaitForEnd());
        }

        /// <summary>
        /// Gets the event dispatcher to use for play events.
        /// </summary>
        /// <returns></returns>
        protected abstract PlayEventDispatcher GetPlayEventDispatcher();

        /// <summary>
        /// Gets the event dispatcher to use for tick events.
        /// </summary>
        /// <returns></returns>
        protected abstract TickEventDispatcher GetTickEventDispatcher();

        /// <summary>
        /// Gets the event dispatcher to use for late tick events.
        /// </summary>
        /// <returns></returns>
        protected abstract LateTickEventDispatcher GetLateTickEventDispatcher();

        /// <summary>
        /// Gets the event dispatcher to use for fixed tick events.
        /// </summary>
        /// <returns></returns>
        protected abstract FixedTickEventDispatcher GetFixedTickEventDispatcher();

        /// <summary>
        /// Get the order to call this behaviour in.
        /// </summary>
        /// <returns>An integer for how to sort this behaviour.</returns>
        public abstract int GetCallOrder();

        /// <summary>
        /// Called when the behaviour is being constructed. This will usually be in editor.
        /// </summary>
        protected virtual void OnConstruct() { }

        /// <summary>
        /// Called when the behaviour is created and setup.
        /// </summary>
        protected abstract void OnSetup();

        /// <summary>
        /// Called when the behaviour becomes an active part of the game.
        /// </summary>
        protected abstract void OnBeginPlay();

        /// <summary>
        /// Called when the behaviour stops being an active part of the game.
        /// </summary>
        protected abstract void OnEndPlay();

        /// <summary>
        /// Called every frame this behaviour is an active part of the game.
        /// </summary>
        protected virtual void OnTick() { }

        /// <summary>
        /// Called every frame this behaviour is an active part of the game after all tick events have occured.
        /// </summary>
        protected virtual void OnLateTick() { }

        /// <summary>
        /// Called every physics frame this behaviour is an active part of the game.
        /// </summary>
        protected virtual void OnFixedTick() { }

        /// <summary>
        /// Called when this behaviour is destroyed.
        /// </summary>
        protected abstract void OnCleanup();
    }
}
