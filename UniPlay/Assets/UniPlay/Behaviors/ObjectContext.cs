﻿using UniPlay.Gameplay;
using UnityEngine;
using UnityEngine.Assertions;

namespace UniPlay.Behaviors
{
    /// <summary>
    /// Represents a type of object and outlines its structure. This does not hold the objects actual behaviour.
    /// </summary>
    public abstract class ObjectContext
        : AdvancedBehavior
    {
        public static T Spawn<T>(T template, ObjectContext instigator, Vector3 position, Quaternion rotation)
            where T : ObjectContext
        {
            T newObject = Instantiate(template, position, rotation) as T;

            Assert.IsNotNull(newObject);

            ObjectContext[] structure = newObject.GetComponentsInChildren<ObjectContext>(true);
            foreach (ObjectContext context in structure)
            {
                context.m_instigator = instigator;
            }

            return newObject;
        }

        public static T Spawn<T>(T template, ObjectContext instigator)
            where T : ObjectContext
        {
            return Spawn(template, instigator, template.transform.position, template.transform.rotation);
        }

        public static T Spawn<T>(T template)
            where T : ObjectContext
        {
            return Spawn(template, null);
        }

        public static T Spawn<T, D>(T template, ObjectContext instigator, Vector3 position, Quaternion rotation, D data)
            where T : ObjectContext
        {
            T newObject = Spawn(template, instigator, position, rotation);

            IConstructable<D> constructable = newObject as IConstructable<D>;
            if (constructable != null)
                constructable.Construct(data);

            return newObject;
        }

        public static T Spawn<T, D>(T template, ObjectContext instigator, D data)
            where T : ObjectContext
        {
            return Spawn(template, instigator, template.transform.position, template.transform.rotation, data);
        }

        public static T Spawn<T, D>(T template, D data)
            where T : ObjectContext
        {
            return Spawn(template, null, data);
        }

        public static T Spawn<T, L>(T template, ObjectContext instigator, Vector3 position, Quaternion rotation)
            where T : ObjectContext
            where L : LifeHandler
        {
            T newObject = Spawn<T>(template, instigator, position, rotation);

            LifeHandler handler = newObject.GetComponent<LifeHandler>();
            if(handler != null)
            {
                Destroy(handler);
            }

            newObject.gameObject.AddComponent<L>();

            return newObject;
        }

        public static T Spawn<T, L>(T template, ObjectContext instigator)
            where T : ObjectContext
            where L : LifeHandler
        {
            return Spawn<T, L>(template, instigator, template.transform.position, template.transform.rotation);
        }

        public static T Spawn<T, L>(T template)
            where T : ObjectContext
            where L : LifeHandler
        {
            return Spawn<T, L>(template, null, template.transform.position, template.transform.rotation);
        }

        public static T Spawn<T, L, D>(T template, ObjectContext instigator, Vector3 position, Quaternion rotation, D data)
            where T : ObjectContext
            where L : LifeHandler
        {
            T newObject = Spawn<T, D>(template, instigator, position, rotation, data);

            LifeHandler handler = newObject.GetComponent<LifeHandler>();
            if (handler == null)
            {
                newObject.gameObject.AddComponent<L>();
            }

            return newObject;
        }

        public static T Spawn<T, L, D>(T template, ObjectContext instigator, D data)
            where T : ObjectContext
            where L : LifeHandler
        {
            return Spawn<T, L, D>(template, instigator, template.transform.position, template.transform.rotation, data);
        }

        public static T Spawn<T, L, D>(T template, D data)
            where T : ObjectContext
            where L : LifeHandler
        {
            return Spawn<T, L, D>(template, null, template.transform.position, template.transform.rotation, data);
        }

        private ObjectContext m_instigator;

        public override Transform Root
        {
            get { return transform; }
        }

        public virtual ObjectContext Instigator
        {
            get { return m_instigator; }
        }

        protected override PlayEventDispatcher GetPlayEventDispatcher()
        {
            return GameInstance.GetInstance().PlayEventDispatcher;
        }

        protected override TickEventDispatcher GetTickEventDispatcher()
        {
            return GameInstance.GetInstance().TickEventDispatcher;
        }

        protected override LateTickEventDispatcher GetLateTickEventDispatcher()
        {
            return GameInstance.GetInstance().LateTickEventDispatcher;
        }

        protected override FixedTickEventDispatcher GetFixedTickEventDispatcher()
        {
            return GameInstance.GetInstance().FixedTickEventDispatcher;
        }

        protected internal override void Start()
        {
            base.Start();

            if(!HasStarted)
            {
                gameObject.SetActive(false);
            }
        }

        protected internal override void InternalBeginPlay()
        {
            gameObject.SetActive(true);

            base.InternalBeginPlay();

            LifeHandler lifeHandler = GetComponent<LifeHandler>();
            if (lifeHandler == null)
            {
                DefaultLifeHandler handler = gameObject.AddComponent<DefaultLifeHandler>();
            }
        }

        protected internal override void InternalEndPlay()
        {
            if(this != null)
            {
                gameObject.SetActive(false);
            }

            base.InternalEndPlay();
        }

        public override int GetCallOrder()
        {
            return 0;
        }
    }
}