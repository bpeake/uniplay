﻿using System;

namespace UniPlay.Behaviors
{
    public class Part
        : Attribute
    {
        private string m_container;

        public string Container
        {
            get { return m_container; }
        }

        public Part()
        {
            m_container = "";
        }

        public Part(string container)
        {
            m_container = container;
        }
    }
}