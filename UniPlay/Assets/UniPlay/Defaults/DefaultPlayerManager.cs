﻿using UniPlay.Player;

namespace UniPlay.Defaults
{
    public class DefaultPlayerManager : PlayerManager
    {
        public override bool AllowPlayerToPlay(PlayerInfo player)
        {
            return true;
        }

        public override bool AllowPlayerToNotPlay(PlayerInfo player)
        {
            return true;
        }

        public override bool AllowPlayerCreation()
        {
            return true;
        }

        public override bool AllowPlayerBinding(PlayerInfo player, IPlayerBinding newBinding)
        {
            return true;
        }

        public override void OnPlayerReady(PlayerInfo player)
        {
            PlayerReady(player);
        }

        public override void OnPlayerNotReady(PlayerInfo player)
        {
            PlayerNotReady(player);
        }

        public override void OnPlayerAdded(PlayerInfo player)
        {
            PlayerAdded(player);
        }

        public override void OnPlayerRemoved(PlayerInfo player)
        {
            PlayerRemoved(player);
        }

        public override void OnPlayerBindingAdded(PlayerInfo player, IPlayerBinding binding)
        {
            PlayerBindingAdded(player, binding);
        }

        public override void OnPlayerBindingRemoved(PlayerInfo player, IPlayerBinding binding)
        {
            PlayerBindingRemoved(player, binding);
        }
    }
}
