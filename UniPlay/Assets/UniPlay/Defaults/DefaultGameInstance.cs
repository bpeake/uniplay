﻿using UniPlay.Gameplay;
using UnityEngine.SceneManagement;

namespace UniPlay.Defaults
{
    public class DefaultGameInstance : GameInstance
    {
        protected override void Awake()
        {
            base.Awake();

            SceneManager.activeSceneChanged += (from, to) =>
            {
                EndPlay();
                BeginPlay();
            };
        }

        private void OnDestroy()
        {
            EndPlay();
        }

        private void Update ()
        {
            if(PlayEventDispatcher.HasStarted)
                Tick();
        }

        private void LateUpdate()
        {
            if(PlayEventDispatcher.HasStarted)
                LateTick();
        }

        private void FixedUpdate()
        {
            if(PlayEventDispatcher.HasStarted)
                FixedTick();
        }
    }
}
