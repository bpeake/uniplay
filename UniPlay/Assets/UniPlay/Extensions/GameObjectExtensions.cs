﻿using UniPlay.Behaviors;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace UniPlay.Extensions
{
    public static class GameObjectExtensions
    {
        public static ObjectContext GetContext(this GameObject go)
        {
            return go.GetComponent<ObjectContext>();
        }

        /// <summary>
        /// Makes a game object persistent until SceneManager.LoadLevel is called.
        /// </summary>
        /// <param name="go">The game object to mark as persistent.</param>
        /// <remarks>Unlike Don't destroy on load, this method will still have objects deleted LoadLevel is called, however will keep objects when levels are loaded additively.</remarks>
        public static void MakePersistentForRun(this GameObject go)
        {
            Scene persistentScene = SceneManager.GetSceneByName("PersistentForRun");
            if (!persistentScene.isLoaded)
                persistentScene = SceneManager.CreateScene("PersistentForRun");

            SceneManager.MoveGameObjectToScene(go, persistentScene);
        }
    }
}
