﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace UniPlay.Reflection
{
    public static class TypeHelper
    {
        public static IEnumerable<FieldInfo> GetAllFields(Type t)
        {
            if (t == null)
                return Enumerable.Empty<FieldInfo>();

            BindingFlags flags = BindingFlags.Public | BindingFlags.NonPublic |
                                 BindingFlags.Static | BindingFlags.Instance |
                                 BindingFlags.DeclaredOnly;
            return t.GetFields(flags).Concat(GetAllFields(t.BaseType));
        }

        public static IEnumerable<FieldInfo> GetAllFieldsWithAttribute(Type t, Type a)
        {
            if (t == null)
                return Enumerable.Empty<FieldInfo>();

            BindingFlags flags = BindingFlags.Public | BindingFlags.NonPublic |
                                 BindingFlags.Static | BindingFlags.Instance |
                                 BindingFlags.DeclaredOnly;
            return t.GetFields(flags)
                .Where(info => info.IsDefined(a, true))
                .Concat(GetAllFieldsWithAttribute(t.BaseType, a));
        }

        public static IEnumerable<FieldInfo> GetAllInstanceFields(Type t)
        {
            if (t == null)
                return Enumerable.Empty<FieldInfo>();

            BindingFlags flags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance |
                                 BindingFlags.DeclaredOnly;
            return t.GetFields(flags).Concat(GetAllInstanceFields(t.BaseType));
        }

        public static IEnumerable<FieldInfo> GetAllInstanceFieldsWithAttribute(Type t, Type a)
        {
            if (t == null)
                return Enumerable.Empty<FieldInfo>();

            BindingFlags flags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance |
                                 BindingFlags.DeclaredOnly;
            return t.GetFields(flags)
                .Where(info => info.IsDefined(a, true))
                .Concat(GetAllInstanceFieldsWithAttribute(t.BaseType, a));
        }

        public static IEnumerable<MethodInfo> GetAllMethods(Type t, bool includePublic,
            bool includeProtected, bool includePrivate)
        {
            if (t == null)
                return Enumerable.Empty<MethodInfo>();

            BindingFlags flags = (includePublic ? BindingFlags.Public : 0) |
                                 (includePrivate || includeProtected ? BindingFlags.NonPublic : 0) |
                                 BindingFlags.Instance | BindingFlags.DeclaredOnly;

            return t.GetMethods(flags)
                .Where(info => (info.IsPrivate && includePrivate) || (info.IsFamily && includeProtected) ||
                               (info.IsPublic && includePublic))
                .Concat(GetAllMethods(t, includePublic, includeProtected, includePrivate));
        }

        private static MethodInfo InternalGetMethod(Type t, string name, Type[] argTypes, ParameterModifier[] parameterModifiers)
        {
            if (t == null)
                return null;

            BindingFlags flags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance |
                                 BindingFlags.DeclaredOnly;

            MethodInfo method = t.GetMethod(name, flags, null, CallingConventions.Any, argTypes, parameterModifiers);

            return method ?? InternalGetMethod(t.BaseType, name, argTypes, parameterModifiers);
        }

        public static MethodInfo GetMethod(Type t, string name, Type[] argTypes, ParameterModifier[] parameterModifiers)
        {
            MethodInfo method = InternalGetMethod(t, name, argTypes, parameterModifiers);

            if(method != null)
                return method;

            foreach (Type type in t.GetInterfaces())
            {
                method = InternalGetMethod(type, name, argTypes, parameterModifiers);
                if(method != null)
                    return method;
            }

            return null;
        }

        public static MethodInfo GetMethod(Type t, string name, Type[] argTypes)
        {
            int argc = argTypes != null ? argTypes.Length : 0;

            if (argc > 0)
            {
                ParameterModifier mod = new ParameterModifier(argTypes.Length);

                for (int i = 0; i < argTypes.Length; i++)
                {
                    mod[i] = false;
                }

                return GetMethod(t, name, argTypes, new[] {mod});
            }
            else
            {
                return GetMethod(t, name, argTypes, null);
            }
        }
    }
}
