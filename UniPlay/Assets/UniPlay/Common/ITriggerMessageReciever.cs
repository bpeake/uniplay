﻿using UnityEngine;
using System.Collections;
using UniPlay.Messages;

/// <summary>
/// A message Receiver that is notified when entering a trigger.
/// </summary>
public interface ITriggerEnterMessageReceiver
    : IMessageReceiver
{
    void OnTriggerEnter(Collider other);
}

/// <summary>
/// A message Receiver that is notified when staying in a trigger.
/// </summary>
public interface ITriggerStayMessageReceiver
    : IMessageReceiver
{
    void OnTriggerStay(Collider other);
}

/// <summary>
/// A message Receiver that is notified when leaving a trigger.
/// </summary>
public interface ITriggerLeaveMessageReceiver
    : IMessageReceiver
{
    void OnTriggerLeave(Collider other);
}


/// <summary>
/// A message Receiver that is notified when entering a 2d trigger.
/// </summary>
public interface ITriggerEnter2DMessageReceiver
    : IMessageReceiver
{
    void OnTriggerEnter2D(Collider2D other);
}

/// <summary>
/// A message Receiver that is notified when staying in a 2d trigger.
/// </summary>
public interface ITriggerStay2DMessageReceiver
    : IMessageReceiver
{
    void OnTriggerStay2D(Collider2D other);
}

/// <summary>
/// A message Receiver that is notifed when leaving a 2d trigger.
/// </summary>
public interface ITriggerLeave2DMessageReceiver
    : IMessageReceiver
{
    void OnTriggerLeave2D(Collider2D other);
}