﻿using UnityEngine;
using System.Collections;
using UniPlay.Messages;

/// <summary>
/// A message Receiver that should be notified by the engine when entering a collision.
/// </summary>
public interface ICollisionEnterMessageReceiver 
    : IMessageReceiver
{
    void OnCollisionEnter(Collision collision);
}

/// <summary>
/// A message Receiver that should be notified by the engine when staying in a collision.
/// </summary>
public interface ICollisionStayMessageReceiver
    : IMessageReceiver
{
    void OnCollisionStay(Collision collision);
}

/// <summary>
/// A message Receiver that should be notified by the engine when leaving a collision.
/// </summary>
public interface ICollisionLeaveMessageReceiver
    : IMessageReceiver
{
    void OnCollisionLeave(Collision collision);
}


/// <summary>
/// A message Receiver that should be notified by the engine when entering a collision.
/// </summary>
public interface ICollision2DEnterMessageReceiver
    : IMessageReceiver
{
    void OnCollisionEnter2D(Collision2D collision);
}

/// <summary>
/// A message Receiver that should be notified by the engine when staying in a collision.
/// </summary>
public interface ICollision2DStayMessageReceiver
    : IMessageReceiver
{
    void OnCollisionStay2D(Collision2D collision);
}

/// <summary>
/// A message Receiver that should be notified by the engine when leaving a collision.
/// </summary>
public interface ICollision2DLeaveMessageReceiver
    : IMessageReceiver
{
    void OnCollisionLeave2D(Collision2D collision);
}
