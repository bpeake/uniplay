﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine.Assertions;

namespace UniPlay.Player
{
    /// <summary>
    /// Table stores the relation between players and there binding.
    /// </summary>
    public sealed class PlayerStore
    {
        private class PlayerBindingSorter : IComparer<IPlayerBinding>
        {
            public int Compare(IPlayerBinding x, IPlayerBinding y)
            {
                int comparedValue = x.GetOrder().CompareTo(y.GetOrder());
                return comparedValue == 0 ? 1 : comparedValue;
            }
        }

        private Dictionary<IPlayerBinding, PlayerInfo> m_playerLookup = new Dictionary<IPlayerBinding, PlayerInfo>();
        private List<IPlayerBinding> m_bindings = new List<IPlayerBinding>();
        private Dictionary<PlayerInfo, List<IPlayerBinding>> m_playerBindingLookup = new Dictionary<PlayerInfo,List<IPlayerBinding>>();

        private HashSet<PlayerInfo> m_inGamePlayers = new HashSet<PlayerInfo>();
        private PlayerBindingSorter m_playerBindingSorter = new PlayerBindingSorter();

        /// <summary>
        /// Marks a player as ready.
        /// </summary>
        /// <param name="player">The player to mark as ready.</param>
        /// <returns>True if the player could be marked as ready.</returns>
        public bool MarkAsPlaying(PlayerInfo player)
        {
            Assert.IsTrue(m_playerBindingLookup.ContainsKey(player));

            if (m_inGamePlayers.Contains(player))
                return false;

            if (!PlayerManager.GetInstance().AllowPlayerToPlay(player))
                return false;

            m_inGamePlayers.Add(player);

            PlayerManager.GetInstance().OnPlayerReady(player);

            return true;
        }

        /// <summary>
        /// Marks a player as not ready.
        /// </summary>
        /// <param name="player">The player to mark as not ready.</param>
        /// <returns>True if the player could be marked as not ready.</returns>
        public bool MarkAsNotPlaying(PlayerInfo player)
        {
            Assert.IsTrue(m_playerBindingLookup.ContainsKey(player));

            if (!m_inGamePlayers.Contains(player))
                return false;

            if (!PlayerManager.GetInstance().AllowPlayerToNotPlay(player))
                return false;

            m_inGamePlayers.Remove(player);

            PlayerManager.GetInstance().OnPlayerNotReady(player);

            return true;
        }

        public bool IsPlaying(PlayerInfo player)
        {
            Assert.IsTrue(m_playerBindingLookup.ContainsKey(player));

            return m_inGamePlayers.Contains(player);
        }

        /// <summary>
        /// Creates a new player representation.
        /// </summary>
        /// <param name="rootBinding">The first binding to give to this player.</param>
        /// <returns>The newly created player or null if either the player or its root binding fail to add.</returns>
        public PlayerInfo CreatePlayer(IPlayerBinding rootBinding)
        {
            if (!PlayerManager.GetInstance().AllowPlayerCreation())
                return null;

            PlayerInfo player = new PlayerInfo(this);
            m_playerBindingLookup.Add(player, new List<IPlayerBinding>());

            if (!PlayerManager.GetInstance().AllowPlayerBinding(player, rootBinding))
            {
                m_playerBindingLookup.Remove(player);
                return null;
            }

            m_playerLookup.Add(rootBinding, player);
        
            m_bindings.Add(rootBinding);
            m_bindings.Sort(m_playerBindingSorter);

            m_playerBindingLookup[player].Add(rootBinding);
            m_playerBindingLookup[player].Sort(m_playerBindingSorter);

            PlayerManager.GetInstance().OnPlayerAdded(player);
            PlayerManager.GetInstance().OnPlayerBindingAdded(player, rootBinding);

            return player;
        }

        /// <summary>
        /// Creates an entry for a player with the specified bindings.
        /// </summary>
        /// <param name="player">The player to enter in the table.</param>
        /// <param name="binding">The binding to give to that player.</param>
        /// <returns>True if the binding was created, false otherwise.</returns>
        public bool AddPlayerBinding(PlayerInfo player, IPlayerBinding binding)
        {
            Assert.IsTrue(m_playerBindingLookup.ContainsKey(player));

            if (m_playerLookup.ContainsKey(binding))
                return false;

            if (!PlayerManager.GetInstance().AllowPlayerBinding(player, binding))
                return false;

            m_playerLookup.Add(binding, player);

            m_bindings.Add(binding);
            m_bindings.Sort(m_playerBindingSorter);

            m_playerBindingLookup[player].Add(binding);
            m_playerBindingLookup[player].Sort(m_playerBindingSorter);

            PlayerManager.GetInstance().OnPlayerBindingAdded(player, binding);

            return true;
        }

        /// <summary>
        /// Removes a player from the table.
        /// </summary>
        /// <param name="player">The player to remove.</param>
        public void RemovePlayer(PlayerInfo player)
        {
            Assert.IsTrue(m_playerBindingLookup.ContainsKey(player));

            List<IPlayerBinding> playerBindings = m_playerBindingLookup[player];

            while (playerBindings.Count > 0)
            {
                IPlayerBinding binding = playerBindings[0];
                m_playerLookup.Remove(binding);
                m_bindings.RemoveAt(0);
                playerBindings.RemoveAt(0);

                PlayerManager.GetInstance().OnPlayerBindingRemoved(player, binding);
            }

            m_playerBindingLookup.Remove(player);

            PlayerManager.GetInstance().OnPlayerRemoved(player);
        }

        /// <summary>
        /// Removes a player binding lookup.
        /// </summary>
        /// <param name="binding">The binding to remove as an entry for the player.</param>
        public void RemovePlayerBinding(IPlayerBinding binding)
        {
            Assert.IsTrue(m_playerLookup.ContainsKey(binding));

            PlayerInfo player = m_playerLookup[binding];
            List<IPlayerBinding> bindings = m_playerBindingLookup[player];
            bindings.Remove(binding);
            m_bindings.Remove(binding);
            m_playerLookup.Remove(binding);

            PlayerManager.GetInstance().OnPlayerBindingRemoved(player, binding);

            if (bindings.Count == 0)
                RemovePlayer(player);
        }

        /// <summary>
        /// Gets a player by one of its bindings.
        /// </summary>
        /// <param name="binding">The binding to get the player from.</param>
        /// <returns>The player that is bound to that binding, or null if the player could not be found.</returns>
        public PlayerInfo GetPlayer(IPlayerBinding binding)
        {
            if(m_playerLookup.ContainsKey(binding))
                return m_playerLookup[binding];

            return null;
        }

        public bool HasPlayer(IPlayerBinding binding)
        {
            return m_playerLookup.ContainsKey(binding);
        }

        /// <summary>
        /// Gets a binding entry of type K from the player.
        /// </summary>
        /// <typeparam name="B">The type of binding to look for.</typeparam>
        /// <param name="player">The player to get the binding from.</param>
        /// <returns>The player binding of that type.</returns>
        public B GetPlayerBindingOfType<B>(PlayerInfo player) 
            where B : IPlayerBinding
        {
            return m_playerBindingLookup[player]
                .OfType<B>()
                .FirstOrDefault();
        }

        /// <summary>
        /// Iterates over all players with a certain binding type.
        /// </summary>
        /// <typeparam name="B">The type of binding.</typeparam>
        /// <returns>An enumerable of the players with that binding type.</returns>
        public IEnumerable<PlayerInfo> IteratePlayersWithBindingType<B>()
            where B : IPlayerBinding
        {
            return m_bindings.OfType<B>()
                .Select(b => m_playerLookup[b])
                .Distinct();
        }

        /// <summary>
        /// Iterates over all players.
        /// </summary>
        /// <returns>An enumerable of all players.</returns>
        public IEnumerable<PlayerInfo> IteratePlayers()
        {
            return m_playerBindingLookup.Keys;
        }

        /// <summary>
        /// Gets the number of players with a certain binding type.
        /// </summary>
        /// <typeparam name="B">Tye type of binding.</typeparam>
        /// <returns>The count of players with a certain binding type.</returns>
        public int GetPlayersWithBindingTypeCount<B>()
            where B : IPlayerBinding
        {
            return IteratePlayersWithBindingType<B>().Count();
        }

        /// <summary>
        /// Iterates over all bindings for a player.
        /// </summary>
        /// <param name="player">The player to get the bindings for.</param>
        /// <returns>An enumerable of the players bindings.</returns>
        public IEnumerable<IPlayerBinding> IteratePlayerBindings(PlayerInfo player)
        {
            var bindings = m_playerBindingLookup[player];
            return bindings.AsEnumerable();
        }

        /// <summary>
        /// Gets the next binding after this binding.
        /// </summary>
        /// <typeparam name="B">The type of binding to use.</typeparam>
        /// <param name="binding">The binding to start at.</param>
        /// <returns>The next binding.</returns>
        public B GetNext<B>(B binding)
            where B : IPlayerBinding
        {
            int index = m_bindings.IndexOf(binding);
            Assert.AreNotEqual(-1, index);

            for (int i = (index + 1) % m_bindings.Count; i != index; i = (i + 1) % m_bindings.Count)
            {
                IPlayerBinding b = m_bindings[i];
                if (b is B)
                {
                    return (B) b;
                }
            }

            return (B) m_bindings[index];
        }

        /// <summary>
        /// Gets the previous binding before this binding.
        /// </summary>
        /// <typeparam name="B">The type of binding to use.</typeparam>
        /// <param name="binding">The binding to start at.</param>
        /// <returns>The previous binding.</returns>
        public B GetPrevious<B>(B binding)
            where B : IPlayerBinding
        {
            int index = m_bindings.IndexOf(binding);
            Assert.AreNotEqual(-1, index);

            for (int i = index - 1; i != index; i--)
            {
                if (i < 0)
                    i = m_bindings.Count - 1;

                IPlayerBinding b = m_bindings[i];
                if (b is B)
                {
                    return (B)b;
                }
            }

            return (B) m_bindings[index];
        }
    }
}
