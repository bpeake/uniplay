﻿namespace UniPlay.Player
{
    public interface IPlayerBinding
    {
        /// <summary>
        /// The order that this binding should be stored in.
        /// </summary>
        /// <returns>A number representing how to sort this binding relative to other bindings.</returns>
        int GetOrder();
    }
}
