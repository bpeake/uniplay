﻿using System.Linq;
using UniPlay.Events;
using UniPlay.Gameplay;
using UnityEngine;
using UnityEngine.Assertions;

namespace UniPlay.Player
{
    /// <summary>
    /// Handles player creation/deletion events.
    /// </summary>
    public abstract class PlayerManager : MonoBehaviour
    {
        private static PlayerManager s_instance;

        private PlayerStore m_store;

        public static void Reset()
        {
            Destroy(s_instance.gameObject);
            Instantiate(GameSettings.GetSettings().PlayerManagerPrefab);
            s_instance.m_store = new PlayerStore();
        }

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
        private static void Startup()
        {
            Assert.IsNotNull(GameSettings.GetSettings().PlayerManagerPrefab);

            Instantiate(GameSettings.GetSettings().PlayerManagerPrefab);
        }

        /// <summary>
        /// The player storage location.
        /// </summary>
        public PlayerStore Store
        {
            get { return m_store; }
        }

        /// <summary>
        /// Gets the player manager instance.
        /// </summary>
        /// <returns>The player manager instance.</returns>
        public static PlayerManager GetInstance()
        {
            return s_instance;
        }

        /// <summary>
        /// Gets the player manager instance.
        /// </summary>
        /// <typeparam name="T">The type of player manager to cast to.</typeparam>
        /// <returns>The player manager instance casted as T or null if a cast could not be done.</returns>
        public static T GetInstance<T>() 
            where T : PlayerManager
        {
            return s_instance as T;
        }

        private void Awake()
        {
            DontDestroyOnLoad(gameObject);

            if (s_instance)
            {
                m_store = s_instance.m_store;
                Destroy(s_instance);
            }
            else
            {
                m_store = new PlayerStore();
            }

            s_instance = this;
        }

        private void OnDestroy()
        {
            PlayerInfo[] players = m_store.IteratePlayers().ToArray();

            foreach (PlayerInfo player in players)
            {
                m_store.RemovePlayer(player);
            }
        }

        /// <summary>
        /// Should this player be allowed to be put into the playing state.
        /// </summary>
        /// <param name="player">The player to attempt to put into the playing state.</param>
        /// <returns>True if it should, false otherwise.</returns>
        public abstract bool AllowPlayerToPlay(PlayerInfo player);

        /// <summary>
        /// Should this player be allowed to be put into the not playing state.
        /// </summary>
        /// <param name="player">The player to put into the not playing state.</param>
        /// <returns>True if it should, false otherwise.</returns>
        public abstract bool AllowPlayerToNotPlay(PlayerInfo player);

        /// <summary>
        /// Should new players be allowed to be added.
        /// </summary>
        /// <returns>True if more players can be added, false otherwise.</returns>
        public abstract bool AllowPlayerCreation();

        /// <summary>
        /// Should this player be allowed to have this binding.
        /// </summary>
        /// <param name="player">The player we are attempting to bind to.</param>
        /// <param name="newBinding">The possible new binding for the player.</param>
        /// <returns>True if the new binding can be added to the player.</returns>
        public abstract bool AllowPlayerBinding(PlayerInfo player, IPlayerBinding newBinding);

        /// <summary>
        /// Called when a player is put into the read state.
        /// </summary>
        /// <param name="player">The player that is now ready.</param>
        public abstract void OnPlayerReady(PlayerInfo player);

        protected void PlayerReady(PlayerInfo player)
        {
            GlobalEventDispatcher.GetDispatcher<PlayerManagerEventDispatcher>().OnPlayerReady(player);
        }

        /// <summary>
        /// Called when a player is not ready.
        /// </summary>
        /// <param name="player">The player that is no longer ready.</param>
        public abstract void OnPlayerNotReady(PlayerInfo player);

        protected void PlayerNotReady(PlayerInfo player)
        {
            GlobalEventDispatcher.GetDispatcher<PlayerManagerEventDispatcher>().OnPlayerNotReady(player);
        }

        /// <summary>
        /// Called when a player is added.
        /// </summary>
        /// <param name="player">The player that was added.</param>
        public abstract void OnPlayerAdded(PlayerInfo player);

        protected void PlayerAdded(PlayerInfo player)
        {
            GlobalEventDispatcher.GetDispatcher<PlayerManagerEventDispatcher>().OnPlayerAdded(player);
        }

        /// <summary>
        /// Called when a player is removed.
        /// </summary>
        /// <param name="player">The player that was removed.</param>
        public abstract void OnPlayerRemoved(PlayerInfo player);

        protected void PlayerRemoved(PlayerInfo player)
        {
            GlobalEventDispatcher.GetDispatcher<PlayerManagerEventDispatcher>().OnPlayerRemoved(player);
        }

        /// <summary>
        /// Called when a player is given a binding.
        /// </summary>
        /// <param name="player">The player that got the binding.</param>
        /// <param name="binding">The new player binding.</param>
        public abstract void OnPlayerBindingAdded(PlayerInfo player, IPlayerBinding binding);

        protected void PlayerBindingAdded(PlayerInfo player, IPlayerBinding binding)
        {
            GlobalEventDispatcher.GetDispatcher<PlayerManagerEventDispatcher>().OnPlayerBindingAdded(player, binding);
        }

        /// <summary>
        /// Called when a player binding is removed.
        /// </summary>
        /// <param name="player">The player that is losing the binding.</param>
        /// <param name="binding">The binding that was removed.</param>
        public abstract void OnPlayerBindingRemoved(PlayerInfo player, IPlayerBinding binding);

        protected void PlayerBindingRemoved(PlayerInfo player, IPlayerBinding binding)
        {
            GlobalEventDispatcher.GetDispatcher<PlayerManagerEventDispatcher>().OnPlayerBindingRemoved(player, binding);
        }
    }
}
