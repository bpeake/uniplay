﻿using System.Collections;
using UniPlay.Events;
using UnityEngine;

namespace UniPlay.Player
{
    /// <summary>
    /// Listener for player events.
    /// </summary>
    public interface IPlayerManagerEventListener : IEventListener<PlayerManagerEventDispatcher>
    {
        void OnPlayerAddedEvent(PlayerInfo player);
        void OnPlayerRemovedEvent(PlayerInfo player);
        void OnPlayerBindingAddedEvent(PlayerInfo player, IPlayerBinding binding);
        void OnPlayerBindingRemovedEvent(PlayerInfo player, IPlayerBinding binding);
        void OnPlayerReadyEvent(PlayerInfo player);
        void OnPlayerNotReadyEvent(PlayerInfo player);
    }

    /// <summary>
    /// Event dispatcher to be fired when player event occur.
    /// </summary>
    public class PlayerManagerEventDispatcher : GlobalEventDispatcher
    {
        public void OnPlayerAdded(PlayerInfo player)
        {
            foreach (IPlayerManagerEventListener listener in IterateListeners<IPlayerManagerEventListener>())
            {
                listener.OnPlayerAddedEvent(player);
            }
        }

        public void OnPlayerRemoved(PlayerInfo player)
        {
            foreach (IPlayerManagerEventListener listener in IterateListeners<IPlayerManagerEventListener>())
            {
                listener.OnPlayerRemovedEvent(player);
            }
        }

        public void OnPlayerBindingAdded(PlayerInfo player, IPlayerBinding binding)
        {
            foreach (IPlayerManagerEventListener listener in IterateListeners<IPlayerManagerEventListener>())
            {
                listener.OnPlayerBindingAddedEvent(player, binding);
            }
        }

        public void OnPlayerBindingRemoved(PlayerInfo player, IPlayerBinding binding)
        {
            foreach (IPlayerManagerEventListener listener in IterateListeners<IPlayerManagerEventListener>())
            {
                listener.OnPlayerBindingRemovedEvent(player, binding);
            }
        }

        public void OnPlayerReady(PlayerInfo player)
        {
            foreach (IPlayerManagerEventListener listener in IterateListeners<IPlayerManagerEventListener>())
            {
                listener.OnPlayerReadyEvent(player);
            }
        }

        public void OnPlayerNotReady(PlayerInfo player)
        {
            foreach (IPlayerManagerEventListener listener in IterateListeners<IPlayerManagerEventListener>())
            {
                listener.OnPlayerNotReadyEvent(player);
            }
        }
    }
}