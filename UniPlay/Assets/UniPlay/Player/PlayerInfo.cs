﻿using System.Linq;
using UnityEngine.Assertions;

namespace UniPlay.Player
{
    public sealed class PlayerInfo
    {
        private PlayerStore m_owner;

        /// <summary>
        /// Gets and sets whether this player is considered playing.
        /// </summary>
        public bool IsPlaying
        {
            get { return m_owner.IsPlaying(this); }
            set
            {
                if (value)
                    m_owner.MarkAsPlaying(this);
                else
                    m_owner.MarkAsNotPlaying(this);
            }
        }

        /// <summary>
        /// The player bindings that belong to this player.
        /// </summary>
        public IPlayerBinding[] PlayerBindings
        {
            get { return m_owner.IteratePlayerBindings(this).ToArray(); }
        }

        private PlayerInfo()
        {
        
        }

        internal PlayerInfo(PlayerStore owner)
        {
            m_owner = owner;
        }

        /// <summary>
        /// Gets a binding of a specific type on this player.
        /// </summary>
        /// <typeparam name="B">The binding type to get.</typeparam>
        /// <returns>The first binding of this type.</returns>
        public B GetBinding<B>()
            where B : IPlayerBinding
        {
            return m_owner.GetPlayerBindingOfType<B>(this);
        }

        /// <summary>
        /// Adds a binding to this player.
        /// </summary>
        /// <param name="binding">The binding to add to this player.</param>
        /// <returns>True if the binding could be added.</returns>
        public bool AddBinding(IPlayerBinding binding)
        {
            return m_owner.AddPlayerBinding(this, binding);
        }

        /// <summary>
        /// Removes a binding from the player.
        /// </summary>
        /// <typeparam name="B">The type of binding to remove from the player.</typeparam>
        public void RemoveBinding<B>()
            where B : IPlayerBinding
        {
            m_owner.RemovePlayerBinding(GetBinding<B>());
        }

        /// <summary>
        /// Removes a binding from the player.
        /// </summary>
        /// <param name="binding">The binding to remove from the player.</param>
        /// <remarks>The binding must be owned by this player.</remarks>
        public void RemoveBinding(IPlayerBinding binding)
        {
            Assert.AreEqual(this, m_owner.GetPlayer(binding));

            m_owner.RemovePlayerBinding(binding);
        }

        /// <summary>
        /// Get the next player.
        /// </summary>
        /// <typeparam name="B">The type of binding to base it off of.</typeparam>
        /// <returns>The next player.</returns>
        public PlayerInfo GetNext<B>()
            where B : IPlayerBinding
        {
            IPlayerBinding defaultBinding = GetBinding<B>();
            IPlayerBinding next;
            for (next = m_owner.GetNext(defaultBinding);
                next != defaultBinding && m_owner.GetPlayer(next) == this;
                next = m_owner.GetNext(next))
            {
            }

            return m_owner.GetPlayer(next);
        }

        /// <summary>
        /// Gets the next player that is playing.
        /// </summary>
        /// <typeparam name="B">The type of binding to base it off of.</typeparam>
        /// <returns>The next playing player.</returns>
        public PlayerInfo GetNextPlaying<B>()
            where B : IPlayerBinding
        {
            Assert.IsTrue(IsPlaying);

            PlayerInfo next;
            for (next = GetNext<B>(); next != this && !next.IsPlaying; next = next.GetNext<B>())
            { }

            return next;
        }

        /// <summary>
        /// Gets the previous player.
        /// </summary>
        /// <typeparam name="B">The type of binding to base it off of.</typeparam>
        /// <returns>The previous player.</returns>
        public PlayerInfo GetPrevious<B>()
            where B : IPlayerBinding
        {
            IPlayerBinding defaultBinding = GetBinding<B>();
            IPlayerBinding prev;
            for (prev = m_owner.GetPrevious(defaultBinding);
                prev != defaultBinding && m_owner.GetPlayer(prev) == this;
                prev = m_owner.GetPrevious(prev))
            {
            }

            return m_owner.GetPlayer(prev);
        }

        /// <summary>
        /// Gets the previous playing player.
        /// </summary>
        /// <typeparam name="B">The type of binding to base it off of.</typeparam>
        /// <returns>The previous playing player.</returns>
        public PlayerInfo GetPreviousPlaying<B>()
            where B : IPlayerBinding
        {
            Assert.IsTrue(IsPlaying);

            PlayerInfo prev;
            for (prev = GetPrevious<B>(); prev != this && !prev.IsPlaying; prev = prev.GetPrevious<B>())
            { }

            return prev;
        }
    }
}
