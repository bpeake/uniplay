﻿using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;

namespace UniPlay.Messages
{
    public delegate void MessageFunctor<T>(T handler) where T : IMessageReceiver;

    public interface IMessageReceiver : IEventSystemHandler
    {
     
    }

    /// <summary>
    /// Class for sending messages to objects.
    /// <remarks>At the moment this is just a wrapper around ExecuteEvents.</remarks>
    /// </summary>
    public static class MessageDispatcher
    {
        /// <summary>
        /// Sends a message to a game object.
        /// </summary>
        /// <typeparam name="T">The type of message handler to get the message.</typeparam>
        /// <param name="target">The game object to send the message to.</param>
        /// <param name="functor">The functor to run on any message handler.</param>
        /// <returns>True if any component on the target object or any component on it's children handled the message.</returns>
        public static bool SendMessage<T>(GameObject target, MessageFunctor<T> functor) 
            where T : IMessageReceiver
        {
            bool didSendMessage = false;
            var transform = target.transform;
            for (int i = 0; i < transform.childCount; i++)
            {
                Transform child = transform.GetChild(i);
                didSendMessage = SendMessage(child.gameObject, functor) || didSendMessage;
            }

            didSendMessage = ExecuteEvents.Execute<T>(transform.gameObject, null, (handler, data) => functor(handler)) || didSendMessage;

            return didSendMessage;
        }
    }
}