﻿using System;

[AttributeUsage(AttributeTargets.Assembly, Inherited = true, AllowMultiple = true)]
public class AssemblyTag : Attribute
{
    public string Tag
    { get; set; }
}