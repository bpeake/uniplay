﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEditor;
using Mono.Cecil;

public static class IlPostProcessorStartup
{
    private class IlPostProcessorGroup
    {
        private int m_groupNumber;

        private LinkedList<IlModulePostProcessor> m_modulePostProcessors = new LinkedList<IlModulePostProcessor>();
        private LinkedList<IlTypePostProcessor> m_typePostProcessors = new LinkedList<IlTypePostProcessor>();
        private LinkedList<IlMethodPostProcessor> m_methodPostProcessors = new LinkedList<IlMethodPostProcessor>();
        private LinkedList<IlPropertyPostProcessor> m_propertyPostProcessors = new LinkedList<IlPropertyPostProcessor>();
        private LinkedList<IlFieldPostProcessor> m_fieldPostProcessors = new LinkedList<IlFieldPostProcessor>();

        public IlPostProcessorGroup(int groupNumber)
        {
            m_groupNumber = groupNumber;
        }

        public void AddPostProcessor(IlPostProcessor postProcessor)
        {
            if (postProcessor is IlModulePostProcessor)
            {
                m_modulePostProcessors.AddLast((IlModulePostProcessor) postProcessor);
            }
            else if (postProcessor is IlTypePostProcessor)
            {
                m_typePostProcessors.AddLast((IlTypePostProcessor) postProcessor);
            }
            else if (postProcessor is IlMethodPostProcessor)
            {
                m_methodPostProcessors.AddLast((IlMethodPostProcessor) postProcessor);
            }
            else if (postProcessor is IlPropertyPostProcessor)
            {
                m_propertyPostProcessors.AddLast((IlPropertyPostProcessor) postProcessor);
            }
            else if (postProcessor is IlFieldPostProcessor)
            {
                m_fieldPostProcessors.AddLast((IlFieldPostProcessor) postProcessor);
            }
            else
            {
                throw new ArgumentException("postProcessor was not of a supported class type");
            }
        }
    } 

    [InitializeOnLoadMethod]
    private static void _startup()
    {
        //_process();
    }

    private static void _process()
    {
        EditorApplication.LockReloadAssemblies();

        bool needsReload = false;
        foreach (Assembly assembly in AppDomain.CurrentDomain.GetAssemblies())
        {
            if (!assembly.Location.Replace('\\', '/')
                .StartsWith(Application.dataPath.Substring(0, Application.dataPath.Length - 7)))
            {
                continue;
            }

            AssemblyTag[] tags = (AssemblyTag[]) assembly.GetCustomAttributes(typeof(AssemblyTag), true);
            if (tags.Length > 0)
            {
                foreach (var tag in tags)
                {
                    if (tag.Tag != "PostProcessed")
                    {
                        needsReload = true;
                        _processAssembly(assembly);
                    }
                }
            }
        }

        EditorApplication.UnlockReloadAssemblies();
    }

    private static void _processAssembly(Assembly assembly)
    {
        AssemblyDefinition assemblyDefinition = AssemblyDefinition.ReadAssembly(assembly.Location);
        
        //Add processed attribute.
        ConstructorInfo assemblyTagConstructorInfo = typeof(AssemblyTag).GetConstructor(Type.EmptyTypes);
        MethodReference assemblyTagCtrReference = assemblyDefinition.MainModule.Import(assemblyTagConstructorInfo);
        CustomAttribute attribute = new CustomAttribute(assemblyTagCtrReference);
        attribute.Properties.Add(
            new Mono.Cecil.CustomAttributeNamedArgument(
                "Tag",
                new CustomAttributeArgument(
                    assemblyDefinition.MainModule.TypeSystem.String, "PostProcessed")));

        //Run the post processing.

    }
}
