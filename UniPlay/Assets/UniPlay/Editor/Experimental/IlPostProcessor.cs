﻿using UnityEngine;
using System.Collections;
using System.Reflection;
using Mono.Cecil;

public abstract class IlPostProcessor
{
}

public abstract class IlPostProcessor<T>
    : IlPostProcessor
{
    public abstract int GetOrder();

    public abstract bool ShouldProcess(T element);

    public abstract void Process(T element);
}

public abstract class IlFieldPostProcessor
    : IlPostProcessor<FieldDefinition>
{
    
}

public abstract class IlPropertyPostProcessor
    : IlPostProcessor<PropertyDefinition>
{

}

public abstract class IlMethodPostProcessor
    : IlPostProcessor<MethodDefinition>
{
    
}

public abstract class IlTypePostProcessor
    : IlPostProcessor<TypeDefinition>
{
    
}

public abstract class IlModulePostProcessor
    : IlPostProcessor<ModuleDefinition>
{
    
}