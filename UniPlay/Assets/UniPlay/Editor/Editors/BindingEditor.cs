﻿using UnityEngine;
using System.Collections;
using UniPlay.Behaviors;
using UnityEditor;

[CustomPropertyDrawer(typeof(BindingAttribute))]
public class BindingEditor : PropertyDrawer
{
    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        if (property.propertyType != SerializedPropertyType.ObjectReference)
        {
            return base.GetPropertyHeight(property, label);
        }

        return base.GetPropertyHeight(property, label);
    }

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        if (property.propertyType != SerializedPropertyType.ObjectReference)
        {
            base.OnGUI(position, property, label);
            return;
        }

        if (property.serializedObject.isEditingMultipleObjects)
        {
            return;
        }

        AdvancedBehavior targetBehavior = property.serializedObject.targetObject as AdvancedBehavior;

        if (!targetBehavior)
        {
            return;
        }

        BindingInfo binding = new BindingInfo(fieldInfo);
        Transform bindContainer = binding.GetBindingContainer(targetBehavior);
        Component currentValue = binding.GetBindValue(targetBehavior);

        Component newComponent =
            EditorGUI.ObjectField(position, string.Format("Bind: {0}", fieldInfo.Name), currentValue,
                fieldInfo.FieldType, true) as Component;

        if (newComponent && newComponent != currentValue)
        {
            PrefabType parentPrefabType = PrefabUtility.GetPrefabType(currentValue.gameObject.transform.root.gameObject);
            PrefabType componentPrefabType = PrefabUtility.GetPrefabType(newComponent.gameObject);

            if (componentPrefabType == PrefabType.Prefab &&
                parentPrefabType != PrefabType.Prefab &&
                parentPrefabType != PrefabType.ModelPrefab)
            {
                GameObject newBinding = PrefabUtility.InstantiatePrefab(newComponent.gameObject) as GameObject;
                if (newBinding != null)
                {
                    newBinding.name = currentValue.gameObject.name;
                    newBinding.transform.parent = currentValue.gameObject.transform.parent;
                    EditorApplication.delayCall += () => { Object.DestroyImmediate(currentValue.gameObject); };
                }
            }
        }
    }
}
