﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Yaml;

public abstract class SerializedContainer
{
}

/// <summary>
/// Represents a container for serialized data.
/// </summary>
public abstract class SerializedContainer<D>
    : SerializedContainer
{
    private Dictionary<string, SerializedContainer> m_innerContainers;

    protected SerializedContainer(D data)
    {
        DoConstruct(data);
    }

    private void DoConstruct(D data)
    {
        m_innerContainers = Construct(data) ?? new Dictionary<string, SerializedContainer>();
    }

    protected abstract Dictionary<string, SerializedContainer> Construct(D data);

    /// <summary>
    /// Gets an inner item from this container.
    /// </summary>
    /// <typeparam name="T">The type of container to get.</typeparam>
    /// <param name="name">The name of the inner container.</param>
    /// <returns>The inner container.</returns>
    public T GetInner<T>(string name)
        where T : SerializedContainer
    {
        SerializedContainer container;
        if (m_innerContainers.TryGetValue(name, out container))
        {
            return container as T;
        }

        return null;
    }

    /// <summary>
    /// Iterates over all inner containers.
    /// </summary>
    /// <typeparam name="T">The type of inner container to iterate over.</typeparam>
    /// <returns>An IEnumerable for iterating over all inner containers of type T.</returns>
    public IEnumerable<T> IterateInner<T>()
        where T : SerializedContainer
    {
        return m_innerContainers.Values.OfType<T>();
    }
}
