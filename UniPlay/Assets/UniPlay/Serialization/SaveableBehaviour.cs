﻿using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;

public class SaveableBehaviour
    : MonoBehaviour,
    ISerializationCallbackReceiver
{
    [SerializeField]
    private string m_serializedData = "";

    [SerializeField]
    private List<Object> m_refsTable = new List<Object>();

    public void OnBeforeSerialize()
    {
    }

    public void OnAfterDeserialize()
    {
    }
}