﻿using System.Collections.Generic;
using System.Linq;
using UniPlay.Behaviors;
using UniPlay.Events;
using UniPlay.Player;
using UnityEngine;

namespace UniPlay.Gameplay
{
    public abstract class GameContext :
        ObjectContext,
        IPlayerManagerEventListener
    {
        private static GameContext s_instance;

        private Dictionary<PlayerInfo, PlayerContext> m_playerContexts = new Dictionary<PlayerInfo, PlayerContext>();

        public abstract PlayerContext DefaultReadyPlayerPrefab { get; }

        public abstract PlayerContext DefaultNotReadyPlayerPrefab { get; }

        /// <summary>
        /// Get the current game context.
        /// </summary>
        /// <returns>The current game context.</returns>
        public static GameContext GetInstance()
        {
            return s_instance;
        }

        /// <summary>
        /// Get the current game context.
        /// </summary>
        /// <typeparam name="T">The type to assume the game context is.</typeparam>
        /// <returns>The current game context.</returns>
        public static T GetInstance<T>()
            where T : GameContext
        {
            return s_instance as T;
        }

        /// <summary>
        /// Gets an iterator for all player contexts.
        /// </summary>
        /// <returns>An iterator for all player contexts.</returns>
        public IEnumerable<PlayerContext> IteratePlayerContexts()
        {
            return m_playerContexts.Values.AsEnumerable();
        }

        /// <summary>
        /// Gets an iterator for all player contexts of a certain type.
        /// </summary>
        /// <typeparam name="T">The type of player context to iterate over.</typeparam>
        /// <returns>An iterator for all player contexts of a certain type.</returns>
        public IEnumerable<T> IteratePlayerContexts<T>()
            where T : PlayerContext
        {
            return IteratePlayerContexts().OfType<T>();
        }

        /// <summary>
        /// Gets an iterator for all player contexts.
        /// </summary>
        /// <typeparam name="T">The type of player context.</typeparam>
        /// <typeparam name="B">The type of binding to sort by.</typeparam>
        /// <returns>An iterator for all player contexts.</returns>
        public IEnumerable<T> IteratePlayerContexts<T, B>()
            where T : PlayerContext
            where B : IPlayerBinding
        {
            return
                PlayerManager.GetInstance()
                    .Store.IteratePlayersWithBindingType<B>()
                    .Select(info => m_playerContexts[info])
                    .OfType<T>();
        }

        /// <summary>
        /// Gets an iterator all player contexts who are in the playing state.
        /// </summary>
        /// <returns>An iterator for all player contexts in the player state.</returns>
        public IEnumerable<PlayerContext> IteratePlayingPlayerContexts()
        {
            return IteratePlayerContexts().Where(context => context.Player.IsPlaying);
        }

        /// <summary>
        /// Gets an iterator for all player contexts of a type who are in the playing state.
        /// </summary>
        /// <typeparam name="T">The type of player context to iterate over.</typeparam>
        /// <returns>An iterator for all player contexts of a type in the playing state.</returns>
        public IEnumerable<T> IteratePlayingPlayerContexts<T>()
            where T : PlayerContext
        {
            return IteratePlayerContexts<T>().Where(context => context.Player.IsPlaying);
        }

        /// <summary>
        /// Gets an iterator for all player contexts of a type who are in the playing state.
        /// </summary>
        /// <typeparam name="T">The type of player context to iterate over.</typeparam>
        /// <typeparam name="B">Tye type of binding to sort by.</typeparam>
        /// <returns>An iterator for all player contexts of a type in the playing state.</returns>
        public IEnumerable<T> IteratePlayingPlayerContexts<T, B>()
            where T : PlayerContext
            where B : IPlayerBinding
        {
            return
                PlayerManager.GetInstance()
                    .Store
                    .IteratePlayersWithBindingType<B>()
                    .Where(info => info.IsPlaying)
                    .Select(info => m_playerContexts[info])
                    .OfType<T>();
        }

        /// <summary>
        /// Gets an iterator for all player contexts not in the playing state.
        /// </summary>
        /// <returns>An iterator for all player contexts not in the playing state.</returns>
        public IEnumerable<PlayerContext> IterateNotPlayingPlayerContexts()
        {
            return IteratePlayerContexts().Where(context => !context.Player.IsPlaying);
        }

        /// <summary>
        /// Gets an iterator for all player contexts of a type who are not in the playing state.
        /// </summary>
        /// <typeparam name="T">The type of player context to iterate over.</typeparam>
        /// <returns>An iterator for all player contexts of a type who are not in the playing state.</returns>
        public IEnumerable<T> IterateNotPlayingPlayerContexts<T>()
            where T : PlayerContext
        {
            return IteratePlayerContexts<T>().Where(context => !context.Player.IsPlaying);
        }

        /// <summary>
        /// Gets an iterator for all player contexts of a type who are not in the playing state.
        /// </summary>
        /// <typeparam name="T">The type of player context to iterate over.</typeparam>
        /// <typeparam name="B">The type of binding to sort on.</typeparam>
        /// <returns>An iterator for all player contexts of a type who are not in the playing state.</returns>
        public IEnumerable<T> IterateNotPlayingPlayerContexts<T, B>()
            where T : PlayerContext
            where B : IPlayerBinding
        {
            return
                PlayerManager.GetInstance()
                    .Store
                    .IteratePlayersWithBindingType<B>()
                    .Where(info => !info.IsPlaying)
                    .Select(info => m_playerContexts[info])
                    .OfType<T>();
        }

        /// <summary>
        /// Get the player context that belongs to a player.
        /// </summary>
        /// <param name="player">The player to get the context for.</param>
        /// <returns>The player context registered to this player.</returns>
        public PlayerContext GetPlayerContext(PlayerInfo player)
        {
            if (player == null)
                return null;

            PlayerContext context;
            if (m_playerContexts.TryGetValue(player, out context))
                return context;

            return null;
        }

        /// <summary>
        /// Get the player context that belongs to a player.
        /// </summary>
        /// <typeparam name="T">The type of player context to cast to.</typeparam>
        /// <param name="player">The player to get the context for.</param>
        /// <returns>The player context registered to this player.</returns>
        public T GetPlayerContext<T>(PlayerInfo player)
            where T : PlayerContext
        {
            return GetPlayerContext(player) as T;
        }

        /// <summary>
        /// Get the player context that belongs to the player who owns this binding.
        /// </summary>
        /// <param name="binding">The binding used to get the player.</param>
        /// <returns>The player context associated with this binding.</returns>
        public PlayerContext GetPlayerContext(IPlayerBinding binding)
        {
            return GetPlayerContext(PlayerManager.GetInstance().Store.GetPlayer(binding));
        }

        /// <summary>
        /// Get the player context that belongs to the player who owns this binding.
        /// </summary>
        /// <typeparam name="T">The type of player context to cast to.</typeparam>
        /// <param name="binding">The binding used to get the player.</param>
        /// <returns>The player context associated with this binding.</returns>
        public T GetPlayerContext<T>(IPlayerBinding binding)
            where T : PlayerContext
        {
            return GetPlayerContext(binding) as T;
        }

        PlayerManagerEventDispatcher IEventListener<PlayerManagerEventDispatcher>.GetDispatcher()
        {
            return GlobalEventDispatcher.GetDispatcher<PlayerManagerEventDispatcher>();
        }

        public override int GetCallOrder()
        {
            return int.MinValue;
        }

        protected override void OnBeginPlay()
        {
            GlobalEventDispatcher.GetDispatcher<PlayerManagerEventDispatcher>().RegisterEventListener(this);

            GameContext oldContext = s_instance;
            s_instance = this;

            if (oldContext)
                m_playerContexts = oldContext.m_playerContexts;

            foreach (PlayerInfo player in PlayerManager.GetInstance().Store.IteratePlayers())
            {
                if (player.IsPlaying)
                    CreateReadyPlayerContext(player);
                else
                    CreateNotReadyPlayerContext(player);
            }
        }

        protected override void OnEndPlay()
        {
            GlobalEventDispatcher.GetDispatcher<PlayerManagerEventDispatcher>().RemoveListener(this);
        }

        public void OnPlayerAddedEvent(PlayerInfo player)
        {
            OnPlayerAdded(player);
        }

        public void OnPlayerRemovedEvent(PlayerInfo player)
        {
            OnPlayerRemoved(player);
        }

        public void OnPlayerBindingAddedEvent(PlayerInfo player, IPlayerBinding binding)
        {
            OnPlayerBindingAdded(player, binding);
        }

        public void OnPlayerBindingRemovedEvent(PlayerInfo player, IPlayerBinding binding)
        {
            OnPlayerBindingRemoved(player, binding);
        }

        public void OnPlayerReadyEvent(PlayerInfo player)
        {
            OnPlayerReady(player);
        }

        public void OnPlayerNotReadyEvent(PlayerInfo player)
        {
            OnPlayerNotReady(player);
        }

        protected virtual void OnPlayerReady(PlayerInfo player)
        {
            CreateReadyPlayerContext(player);
        }

        protected virtual void OnPlayerNotReady(PlayerInfo player)
        {
            CreateNotReadyPlayerContext(player);
        }

        protected virtual void OnPlayerAdded(PlayerInfo player)
        {
        }

        protected virtual void OnPlayerRemoved(PlayerInfo player)
        {
            PlayerContext context;

            if (m_playerContexts.TryGetValue(player, out context))
            {
                context.EndPlay();
            }
        }

        protected virtual void OnPlayerBindingAdded(PlayerInfo player, IPlayerBinding binding)
        {
            if (!m_playerContexts.ContainsKey(player))
            {
                if (player.IsPlaying)
                {
                    CreateReadyPlayerContext(player);
                }
                else
                {
                    CreateNotReadyPlayerContext(player);
                }
            }
        }

        protected virtual void OnPlayerBindingRemoved(PlayerInfo player, IPlayerBinding binding)
        {
        }

        protected virtual PlayerContext CreateReadyPlayerContext(PlayerInfo player)
        {
            return SpawnPlayerContext(DefaultReadyPlayerPrefab, player);
        }

        protected virtual PlayerContext CreateNotReadyPlayerContext(PlayerInfo player)
        {
            return SpawnPlayerContext(DefaultNotReadyPlayerPrefab, player);
        }

        protected PlayerContext SpawnPlayerContext(PlayerContext template, PlayerInfo player)
        {
            PlayerContext oldContext;
            PlayerContext newContext;

            m_playerContexts.TryGetValue(player, out oldContext);

            if (template == null)
            {
                m_playerContexts.Remove(player);

                if (oldContext != null)
                {
                    Destroy(oldContext.gameObject);
                }

                return null;
            }

            if (oldContext != null)
            {
                newContext = Spawn<PlayerContext, DefaultLifeHandler, PlayerContext>(template, this, oldContext);
                Destroy(oldContext.gameObject);
                m_playerContexts.Remove(player);
            }
            else
            {
                newContext = Spawn<PlayerContext, DefaultLifeHandler, PlayerInfo>(template, this, player);
            }

            m_playerContexts.Add(player, newContext);

            return newContext;
        }
    }
}
