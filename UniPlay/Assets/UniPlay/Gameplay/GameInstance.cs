﻿using UniPlay.Extensions;
using UniPlay.Pausing;
using UnityEngine;

namespace UniPlay.Gameplay
{
    /// <summary>
    /// Represents the overall game.
    /// </summary>
    public abstract class GameInstance : MonoBehaviour
    {
        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
        private static void Startup()
        {
            Instantiate(GameSettings.GetSettings().GameInstancePrefab);
        }

        private static GameInstance s_instance;

        private readonly PlayEventDispatcher m_playEventDispatcher = new PlayEventDispatcher();
        private readonly TickEventDispatcher m_tickEventDispatcher = new TickEventDispatcher();
        private readonly LateTickEventDispatcher m_lateTickEventDispatcher = new LateTickEventDispatcher();
        private readonly FixedTickEventDispatcher m_fixedTickEventDispatcher = new FixedTickEventDispatcher();

        private PauseEventDispatcher m_globalPauseLayer = new PauseEventDispatcher();

        private bool m_hasStarted;

        public PlayEventDispatcher PlayEventDispatcher
        {
            get { return m_playEventDispatcher; }
        }

        public TickEventDispatcher TickEventDispatcher
        {
            get { return m_tickEventDispatcher; }
        }

        public LateTickEventDispatcher LateTickEventDispatcher
        {
            get { return m_lateTickEventDispatcher; }
        }

        public FixedTickEventDispatcher FixedTickEventDispatcher
        {
            get { return m_fixedTickEventDispatcher; }
        }

        public PauseEventDispatcher GlobalPauseLayer
        {
            get { return m_globalPauseLayer; }
        }

        public static GameInstance GetInstance()
        {
            return s_instance;
        }

        public static T GetInstance<T>() where T : GameInstance
        {
            return s_instance as T;
        }

        protected virtual void Awake()
        { 
            s_instance = this;
            gameObject.hideFlags = HideFlags.HideAndDontSave;
            DontDestroyOnLoad(gameObject);
        }

        public void BeginPlay()
        {
            PlayEventDispatcher.OnPlayStart();
        }

        public void EndPlay()
        {
            PlayEventDispatcher.OnPlayEnd();
        }

        public void Tick()
        {
            TickEventDispatcher.Tick();
        }

        public void LateTick()
        {
            LateTickEventDispatcher.Tick();
        }

        public void FixedTick()
        {
            FixedTickEventDispatcher.Tick();
        }

        public void LoadLevel(string levelName)
        {
        
        }

        public void LoadLevel(int levelIndex)
        {
        
        }

        public void StreamLevel(string levelName)
        {
        
        }

        public void StreamLevel(int levelIndex)
        {
        
        }

        public void Pause()
        {
            m_globalPauseLayer.OnPause(true, false, true);
        }

        public void Unpause()
        {
            m_globalPauseLayer.OnUnpause();
        }
    }
}
