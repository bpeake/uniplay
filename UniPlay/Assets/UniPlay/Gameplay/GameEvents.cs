﻿using System.Collections;
using System.Linq;
using UniPlay.Events;
using UnityEngine;

namespace UniPlay.Gameplay
{
    /// <summary>
    /// An event dispatcher to give out play start/stop events.
    /// </summary>
    public class PlayEventDispatcher : EventDispatcher
    {
        private bool m_hasStarted;

        public bool HasStarted
        {
            get { return m_hasStarted; }
        }

        public void OnPlayStart()
        {
            if (m_hasStarted)
                return;

            m_hasStarted = true;

            foreach (IPlayEventListener listener in IterateListeners<IPlayEventListener>())
            {
                listener.OnBeginPlayEvent();
            }
        }

        public void OnPlayEnd()
        {
            if (!m_hasStarted)
                return;

            m_hasStarted = false;

            foreach (IPlayEventListener listener in IterateListeners<IPlayEventListener>())
            {
                listener.OnEndPlayEvent();
            }
        }
    }

    public interface IPlayEventListener : IEventListener<PlayEventDispatcher>
    {
        void OnBeginPlayEvent();
        void OnEndPlayEvent();
    }
}