﻿using UniPlay.Player;
using UnityEditor;
using UnityEditor.Callbacks;
using UnityEngine;
using UnityEngine.Assertions;
#if UNITY_EDITOR

#endif

namespace UniPlay.Gameplay
{
    /// <summary>
    /// Contains settings about the game.
    /// </summary>
    public class GameSettings : ScriptableObject
    {
        [SerializeField]
        private GameInstance m_gameInstancePrefab;

        [SerializeField]
        private PlayerManager m_playerManager;

        public GameInstance GameInstancePrefab
        {
            get { return m_gameInstancePrefab; }
        }

        public PlayerManager PlayerManagerPrefab
        {
            get { return m_playerManager; }
        }

        /// <summary>
        /// Gets the game settings from the resource folder.
        /// </summary>
        /// <returns></returns>
        public static GameSettings GetSettings()
        {
            GameSettings loadedSettings = Resources.Load<GameSettings>("Settings");

            Assert.IsNotNull(loadedSettings);

            return loadedSettings;
        }

#if UNITY_EDITOR
        [InitializeOnLoadMethod, DidReloadScripts]
        private static void EditorSetup()
        {
            GameSettings settings = AssetDatabase.LoadAssetAtPath<GameSettings>("Assets/Resources/Settings.asset");

            if (!settings)
            {
                Debug.Log("Creating default settings.");

                settings = CreateInstance<GameSettings>();

                if(!AssetDatabase.IsValidFolder("Assets/Resources"))
                    AssetDatabase.CreateFolder("Assets", "Resources");

                AssetDatabase.CreateAsset(settings, "Assets/Resources/Settings.asset");
            }
        }
#endif
    }
}
