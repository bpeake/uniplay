﻿using UniPlay.Behaviors;
using UniPlay.Player;
using UnityEngine.Assertions;

namespace UniPlay.Gameplay
{
    public abstract class PlayerContext : 
        ObjectContext,
        IConstructable<PlayerInfo>,
        IConstructable<PlayerContext>
    {
        private PlayerInfo m_player;

        public PlayerInfo Player
        {
            get { return m_player; }
        }

        public PlayerContext GetNext<B>()
            where B : IPlayerBinding
        {
            return GameContext.GetInstance().GetPlayerContext(m_player.GetNext<B>());
        }

        public T GetNext<T, B>()
            where T : PlayerContext
            where B : IPlayerBinding
        {
            return GameContext.GetInstance().GetPlayerContext(m_player.GetNext<B>()) as T;
        }

        public PlayerContext GetPrevious<B>()
            where B : IPlayerBinding
        {
            return GameContext.GetInstance().GetPlayerContext(m_player.GetPrevious<B>());
        }

        public T GetPrevious<T, B>()
            where T : PlayerContext
            where B : IPlayerBinding
        {
            return GameContext.GetInstance().GetPlayerContext(m_player.GetPrevious<B>()) as T;
        }

        public PlayerContext GetNextPlaying<B>()
            where B : IPlayerBinding
        {
            return GameContext.GetInstance().GetPlayerContext(m_player.GetNextPlaying<B>());
        }

        public T GetNextPlaying<T, B>()
            where T : PlayerContext
            where B : IPlayerBinding
        {
            return GameContext.GetInstance().GetPlayerContext(m_player.GetNextPlaying<B>()) as T;
        }

        public PlayerContext GetPreviousPlaying<B>()
            where B : IPlayerBinding
        {
            return GameContext.GetInstance().GetPlayerContext(m_player.GetPreviousPlaying<B>());
        }

        public T GetPreviousPlaying<T, B>()
            where T : PlayerContext
            where B : IPlayerBinding
        {
            return GameContext.GetInstance().GetPlayerContext(m_player.GetPreviousPlaying<B>()) as T;
        }

        public virtual void Construct(PlayerInfo param)
        {
            m_player = param;
        }

        public virtual void Construct(PlayerContext param)
        {
            m_player = param.m_player;
        }

        public override int GetCallOrder()
        {
            return -9000;
        }
    }
}
