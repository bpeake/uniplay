﻿using System.Collections;
using UniPlay.Events;
using UnityEngine;

namespace UniPlay.Gameplay
{
    public interface ITickEventListener : IEventListener<TickEventDispatcher>
    {
        void OnTickEvent();
    }

    public class TickEventDispatcher : EventDispatcher
    {
        public void Tick()
        {
            foreach (ITickEventListener listener in IterateListeners<ITickEventListener>())
            {
                listener.OnTickEvent();
            }
        }
    }

    public interface ILateTickEventListener : IEventListener<LateTickEventDispatcher>
    {
        void OnLateTickEvent();
    }

    public class LateTickEventDispatcher : EventDispatcher
    {
        public void Tick()
        {
            foreach (ILateTickEventListener listener in IterateListeners<ILateTickEventListener>())
            {
                listener.OnLateTickEvent();
            }
        }
    }

    public interface IFixedTickEventListener : IEventListener<FixedTickEventDispatcher>
    {
        void OnFixedTickEvent();
    }

    public class FixedTickEventDispatcher : EventDispatcher
    {
        public void Tick()
        {
            foreach (IFixedTickEventListener listener in IterateListeners<IFixedTickEventListener>())
            {
                listener.OnFixedTickEvent();
            }
        }
    }
}