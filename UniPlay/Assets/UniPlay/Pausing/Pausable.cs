﻿using System.Collections.Generic;
using System.Linq;
using UniPlay.Behaviors;
using UniPlay.Events;
using UniPlay.Gameplay;
using UnityEngine;
using UnityEngine.Assertions;

namespace UniPlay.Pausing
{
    /// <summary>
    /// This object is pausable.
    /// </summary>
    public class Pausable
        : SubBehavior<ObjectContext>,
            IPauseEventListener
    {
        //Store the data for each pause layer.
        private Dictionary<PauseEventDispatcher, PauseData> m_pauseLayerData =
            new Dictionary<PauseEventDispatcher, PauseData>();

        private HashSet<PauseEventDispatcher> m_registeredPauseLayers = new HashSet<PauseEventDispatcher>();

        private bool m_wasTicking, m_wasLateTicking, m_wasFixedTicking;

        private Rigidbody[] m_rigidbodies = new Rigidbody[0];
        private Vector3[] m_velocities = new Vector3[0];
        private Vector3[] m_angularVelocities = new Vector3[0];
        private RigidbodyConstraints[] m_constraints = new RigidbodyConstraints[0];

        private Rigidbody2D[] m_rigidbody2Ds = new Rigidbody2D[0];
        private Vector2[] m_velocities2D = new Vector2[0];
        private float[] m_angularVelocities2D = new float[0];
        private RigidbodyConstraints2D[] m_constraints2Ds = new RigidbodyConstraints2D[0];

        private Animator[] m_animators = new Animator[0];
        private float[] m_animatorSpeed = new float[0];

        private TrailRenderer[] m_trails = new TrailRenderer[0];
        private float[] m_trailTimes = new float[0];

        private Collider[] m_colliders = new Collider[0];
        private bool[] m_enabledColliders = new bool[0];

        private Collider2D[] m_collider2Ds = new Collider2D[0];
        private bool[] m_enabledColliders2D = new bool[0];

        private AudioSource[] m_audioSources = new AudioSource[0];
        private bool[] m_sourcePlaying = new bool[0];

        private ParticleSystem[] m_particleSystems = new ParticleSystem[0];
        private bool[] m_particlePlaying = new bool[0];

        private int GetPauseLayerCount()
        {
            return m_pauseLayerData.Count;
        }

        private int GetColliderPauseLayerCount()
        {
            return m_pauseLayerData.Values.Count(data => data.StopCollision);
        }

        private int GetAudioPauseLayerCount()
        {
            return m_pauseLayerData.Values.Count(data => data.StopAudio);
        }

        private int GetParticlePauseLayerCount()
        {
            return m_pauseLayerData.Values.Count(data => data.StopParticles);
        }

        private void PauseBaseObject()
        {
            if (GetPauseLayerCount() == 1)
            {
                m_rigidbodies = GetComponentsInChildren<Rigidbody>(true).Where(r => !r.isKinematic).ToArray();
                m_velocities = new Vector3[m_rigidbodies.Length];
                m_angularVelocities = new Vector3[m_rigidbodies.Length];
                m_constraints = new RigidbodyConstraints[m_rigidbodies.Length];

                m_rigidbody2Ds = GetComponentsInChildren<Rigidbody2D>(true).Where(r => !r.isKinematic).ToArray();
                m_velocities2D = new Vector2[m_rigidbody2Ds.Length];
                m_angularVelocities2D = new float[m_rigidbody2Ds.Length];
                m_constraints2Ds = new RigidbodyConstraints2D[m_rigidbody2Ds.Length];

                m_animators = GetComponentsInChildren<Animator>(true);
                m_animatorSpeed = new float[m_animators.Length];

                m_trails = GetComponentsInChildren<TrailRenderer>(true);
                m_trailTimes  = new float[m_trails.Length];

                m_wasTicking = Owner.IsListening<ITickEventListener, TickEventDispatcher>();
                Owner.Mute<ITickEventListener, TickEventDispatcher>();

                m_wasLateTicking = Owner.IsListening<ILateTickEventListener, LateTickEventDispatcher>();
                Owner.Mute<ILateTickEventListener, LateTickEventDispatcher>();

                m_wasFixedTicking = Owner.IsListening<IFixedTickEventListener, FixedTickEventDispatcher>();
                Owner.Mute<IFixedTickEventListener, FixedTickEventDispatcher>();

                for (int i = 0; i < m_rigidbodies.Length; i++)
                {
                    Rigidbody rigidbody = m_rigidbodies[i];
                    m_velocities[i] = rigidbody.velocity;
                    m_angularVelocities[i] = rigidbody.angularVelocity;
                    m_constraints[i] = rigidbody.constraints;
                    rigidbody.constraints = RigidbodyConstraints.FreezeAll;
                }

                for (int i = 0; i < m_rigidbody2Ds.Length; i++)
                {
                    Rigidbody2D rigidbody = m_rigidbody2Ds[i];
                    m_velocities2D[i] = rigidbody.velocity;
                    m_angularVelocities2D[i] = rigidbody.angularVelocity;
                    m_constraints2Ds[i] = rigidbody.constraints;
                    rigidbody.constraints = RigidbodyConstraints2D.FreezeAll;
                }

                for (int i = 0; i < m_animators.Length; i++)
                {
                    Animator animator = m_animators[i];
                    m_animatorSpeed[i] = animator.speed;
                    animator.speed = 0.0f;
                }

                for (int i = 0; i < m_trails.Length; i++)
                {
                    TrailRenderer trail = m_trails[i];
                    m_trailTimes[i] = trail.time;
                    trail.time = float.PositiveInfinity;
                }
            }
        }

        private void PauseCollision()
        {
            if (GetColliderPauseLayerCount() == 1)
            {
                m_colliders = GetComponentsInChildren<Collider>(true);
                m_collider2Ds = GetComponentsInChildren<Collider2D>(true);
                m_enabledColliders = new bool[m_colliders.Length];
                m_enabledColliders2D = new bool[m_collider2Ds.Length];

                for (int i = 0; i < m_colliders.Length; i++)
                {
                    Collider collider = m_colliders[i];
                    m_enabledColliders[i] = collider.enabled;
                    collider.enabled = false;
                }

                for (int i = 0; i < m_collider2Ds.Length; i++)
                {
                    Collider2D collider = m_collider2Ds[i];
                    m_enabledColliders2D[i] = collider.enabled;
                    collider.enabled = false;
                }
            }
        }

        private void PauseAudio()
        {
            if (GetAudioPauseLayerCount() == 1)
            {
                m_audioSources = GetComponentsInChildren<AudioSource>(true);
                m_sourcePlaying = new bool[m_audioSources.Length];

                for (int i = 0; i < m_audioSources.Length; i++)
                {
                    AudioSource source = m_audioSources[i];
                    m_sourcePlaying[i] = source.isPlaying;
                    source.Pause();
                }
            }
        }

        private void PauseParticles()
        {
            if (GetParticlePauseLayerCount() == 1)
            {
                m_particleSystems = GetComponentsInChildren<ParticleSystem>(true);
                m_particlePlaying = new bool[m_particleSystems.Length];

                for (int i = 0; i < m_particleSystems.Length; i++)
                {
                    ParticleSystem system = m_particleSystems[i];
                    m_particlePlaying[i] = system.isPlaying;
                    system.Pause(true);
                }
            }
        }

        private void UnpauseBaseObject()
        {
            if (GetPauseLayerCount() == 0)
            {
                if(m_wasTicking)
                    Owner.Listen<ITickEventListener, TickEventDispatcher>();

                if(m_wasLateTicking)
                    Owner.Listen<ILateTickEventListener, LateTickEventDispatcher>();

                if(m_wasFixedTicking)
                    Owner.Listen<IFixedTickEventListener, FixedTickEventDispatcher>();

                for (int i = 0; i < m_rigidbodies.Length; i++)
                {
                    Rigidbody rigidbody = m_rigidbodies[i];
                    rigidbody.constraints = m_constraints[i];
                    rigidbody.velocity = m_velocities[i];
                    rigidbody.angularVelocity = m_angularVelocities[i];
                    rigidbody.WakeUp();
                }
                m_rigidbodies = new Rigidbody[0];
                m_velocities = new Vector3[0];
                m_angularVelocities = new Vector3[0];
                m_constraints = new RigidbodyConstraints[0];

                for (int i = 0; i < m_rigidbody2Ds.Length; i++)
                {
                    Rigidbody2D rigidbody = m_rigidbody2Ds[i];
                    rigidbody.constraints = m_constraints2Ds[i];
                    rigidbody.velocity = m_velocities2D[i];
                    rigidbody.angularVelocity = m_angularVelocities2D[i];
                    rigidbody.WakeUp();
                }
                m_rigidbody2Ds = new Rigidbody2D[0];
                m_velocities2D = new Vector2[0];
                m_angularVelocities2D = new float[0];
                m_constraints2Ds = new RigidbodyConstraints2D[0];

                for (int i = 0; i < m_animators.Length; i++)
                {
                    Animator animator = m_animators[i];
                    animator.speed = m_animatorSpeed[i];
                }
                m_animators = new Animator[0];
                m_animatorSpeed = new float[0];

                for (int i = 0; i < m_trails.Length; i++)
                {
                    TrailRenderer trail = m_trails[i];
                    trail.time = m_trailTimes[i];
                }
                m_trails = new TrailRenderer[0];
                m_trailTimes = new float[0];
            }
        }

        private void UnpauseCollision()
        {
            if (GetColliderPauseLayerCount() == 0)
            {
                for (int i = 0; i < m_colliders.Length; i++)
                {
                    Collider collider = m_colliders[i];
                    collider.enabled = m_enabledColliders[i];
                }
                m_colliders = new Collider[0];
                m_enabledColliders = new bool[0];

                for (int i = 0; i < m_collider2Ds.Length; i++)
                {
                    Collider2D collider = m_collider2Ds[i];
                    collider.enabled = m_enabledColliders2D[i];
                }
                m_collider2Ds = new Collider2D[0];
                m_enabledColliders2D = new bool[0];
            }
        }

        private void UnpauseAudio()
        {
            if (GetAudioPauseLayerCount() == 0)
            {
                for (int i = 0; i < m_audioSources.Length; i++)
                {
                    AudioSource source = m_audioSources[i];
                    if (m_sourcePlaying[i])
                        source.Play();
                }
                m_audioSources = new AudioSource[0];
                m_sourcePlaying = new bool[0];
            }
        }

        private void UnpauseParticles()
        {
            if (GetParticlePauseLayerCount() == 0)
            {
                for (int i = 0; i < m_particleSystems.Length; i++)
                {
                    ParticleSystem system = m_particleSystems[i];
                    if (m_particlePlaying[i])
                        system.Play(true);
                }
                m_particleSystems = new ParticleSystem[0];
                m_particlePlaying = new bool[0];
            }
        }

        protected override void OnSetup()
        {
        }

        protected override void OnBeginPlay()
        {
            RegisterPauseLayer(GameInstance.GetInstance().GlobalPauseLayer);

            foreach (PauseEventDispatcher layer in m_registeredPauseLayers)
            {
                layer.RegisterEventListener(this);

                if (layer.IsPaused)
                {
                    PauseData data = layer.LastPauseData;
                    OnPause(ref data);
                }
            }
        }

        protected override void OnEndPlay()
        {
            foreach (PauseEventDispatcher layer in m_registeredPauseLayers)
            {
                if (layer.IsPaused)
                {
                    OnUnpause(layer);
                }

                layer.RemoveListener(this);
            }
        }

        protected override void OnCleanup()
        {
        }

        /// <summary>
        /// Registers a pause layer that this object should listen to.
        /// </summary>
        /// <param name="pauseLayer">The pause layer to listen to.</param>
        public void RegisterPauseLayer(PauseEventDispatcher pauseLayer)
        {
            m_registeredPauseLayers.Add(pauseLayer);
        }

        /// <summary>
        /// Unregisters a pause layer that this object should listen to.
        /// </summary>
        /// <param name="pauseLayer"></param>
        public void UnregisterPauseLayer(PauseEventDispatcher pauseLayer)
        {
            m_registeredPauseLayers.Remove(pauseLayer);
        }

        public void OnPause(ref PauseData pauseData)
        {
            Assert.IsFalse(m_pauseLayerData.ContainsKey(pauseData.Instigator));
            m_pauseLayerData.Add(pauseData.Instigator, pauseData);

            PauseBaseObject();
            PauseCollision();
            PauseAudio();
            PauseParticles();
        }

        public void OnUnpause(PauseEventDispatcher instigator)
        {
            Assert.IsTrue(m_pauseLayerData.ContainsKey(instigator));
            m_pauseLayerData.Remove(instigator);

            UnpauseBaseObject();
            UnpauseCollision();
            UnpauseAudio();
            UnpauseParticles();
        }

        public override int GetCallOrder()
        {
            return int.MaxValue;
        }

        PauseEventDispatcher IEventListener<PauseEventDispatcher>.GetDispatcher()
        {
            return null;
        }
    }
}
