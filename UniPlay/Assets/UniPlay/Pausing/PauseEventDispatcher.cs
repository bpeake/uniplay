﻿using System.Collections;
using UniPlay.Events;
using UnityEngine;

namespace UniPlay.Pausing
{
    public struct PauseData
    {
        private PauseEventDispatcher m_instigator;
        private bool m_stopCollision;
        private bool m_stopAudio;
        private bool m_stopParticles;

        public PauseData(PauseEventDispatcher instigator, bool stopCollision, bool stopAudio, bool stopParticles) : this()
        {
            m_instigator = instigator;
            m_stopCollision = stopCollision;
            m_stopAudio = stopAudio;
            m_stopParticles = stopParticles;
        }

        public PauseEventDispatcher Instigator
        {
            get { return m_instigator; }
        }

        public bool StopCollision
        {
            get { return m_stopCollision; }
        }

        public bool StopAudio
        {
            get { return m_stopAudio; }
        }

        public bool StopParticles
        {
            get { return m_stopParticles; }
        }
    }

    public interface IPauseEventListener 
        : IEventListener<PauseEventDispatcher>
    {
        void OnPause(ref PauseData pauseData);
        void OnUnpause(PauseEventDispatcher instigator);
    }

    /// <summary>
    /// Dispatcher for pause events.
    /// </summary>
    public class PauseEventDispatcher : EventDispatcher
    {
        private bool m_isPaused;

        private PauseData m_lastPauseData;

        /// <summary>
        /// Checks to see if the pause event dispatcher is currently paused.
        /// </summary>
        public bool IsPaused
        {
            get { return m_isPaused; }
        }

        /// <summary>
        /// The last pause data to pause with.
        /// </summary>
        public PauseData LastPauseData
        {
            get { return m_lastPauseData; }
        }

        public void OnPause(bool stopCollision, bool stopAudio, bool stopParticles)
        {
            if (m_isPaused)
                return;

            m_isPaused = true;

            m_lastPauseData = new PauseData(this, stopCollision, stopAudio, stopParticles);

            foreach (IPauseEventListener listener in IterateListeners<IPauseEventListener>())
            {
                listener.OnPause(ref m_lastPauseData);
            }
        }

        public void OnUnpause()
        {
            if (!m_isPaused)
                return;

            m_isPaused = false;

            foreach (IPauseEventListener listener in IterateListeners<IPauseEventListener>())
            {
                listener.OnUnpause(this);
            }
        }
    }
}