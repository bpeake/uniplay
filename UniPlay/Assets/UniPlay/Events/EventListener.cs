﻿namespace UniPlay.Events
{
    public interface IEventListener
    {
        int GetCallOrder();
    }

    public interface IEventListener<T>
        : IEventListener
        where T : EventDispatcher
    {
        T GetDispatcher();
    }
}