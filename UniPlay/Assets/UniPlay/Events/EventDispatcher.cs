﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine.Assertions;

namespace UniPlay.Events
{
    /// <summary>
    /// An object for dispatching events to listeners.
    /// </summary>
    public abstract class EventDispatcher
    {
        public delegate void ExecuteDelegate<T>(T listener) where T : IEventListener;

        private LinkedList<IEventListener> m_listeners;
        private HashSet<IEventListener> m_listenerSet;

        private bool m_isUnsafeToModify;
        private LinkedList<IEventListener> m_addingQueue;
        private LinkedList<IEventListener> m_removingQueue;

        protected EventDispatcher()
        {
            m_listeners = new LinkedList<IEventListener>();
            m_addingQueue = new LinkedList<IEventListener>();
            m_removingQueue = new LinkedList<IEventListener>();
            m_listenerSet = new HashSet<IEventListener>();
        }

        /// <summary>
        /// Registers an event listener with this dispatcher.
        /// </summary>
        /// <param name="listener">The listener to register.</param>
        public void RegisterEventListener(IEventListener listener)
        {
            if (m_listenerSet.Contains(listener))
                return;

            m_listenerSet.Add(listener);

            if (!m_isUnsafeToModify)
            {
                m_listeners.AddLast(listener);
            
            }
            else
            {
                m_listeners.AddLast(listener);
            }   
        }

        /// <summary>
        /// Removes an event listener from this dispatcher.
        /// </summary>
        /// <param name="listener">The listener to add to this dispatcher.</param>
        public void RemoveListener(IEventListener listener)
        {
            if (m_listenerSet.Contains(listener))
            {
                m_listenerSet.Remove(listener);

                if (!m_isUnsafeToModify)
                {
                    m_listeners.Remove(listener);
                }
                else
                {
                    m_removingQueue.AddLast(listener);
                }
            }
        }

        /// <summary>
        /// Checks to see if this dispatcher contains a listener.
        /// </summary>
        /// <param name="listener">The listener to check for.</param>
        /// <returns>True if the listener is registered with this dispatcher, false otherwise.</returns>
        public bool HasListener(IEventListener listener)
        {
            return m_listenerSet.Contains(listener);
        }

        /// <summary>
        /// Executes a command on all listeners.
        /// </summary>
        /// <typeparam name="T">The type of listener to execute on.</typeparam>
        /// <param name="command">The command to run on all listeners.</param>
        public void Execute<T>(ExecuteDelegate<T> command) where T : IEventListener
        {
            foreach (T listener in IterateListeners<T>())
            {
                command(listener);
            }
        }

        /// <summary>
        /// Iterates through the registered listeners for this dispatcher.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        protected IEnumerable<T> IterateListeners<T>() where T : IEventListener
        {
            m_isUnsafeToModify = true;

            foreach (IEventListener eventListener in m_listeners.OrderBy(listener => listener.GetCallOrder()))
            {
                if (eventListener is T)
                {
                    yield return (T) eventListener;
                }
            }

            m_isUnsafeToModify = false;

            _cleanQueues();
        }

        private void _cleanQueues()
        {
            Assert.IsFalse(m_isUnsafeToModify);

            foreach (IEventListener listener in m_removingQueue)
            {
                m_listeners.Remove(listener);
            }
            m_removingQueue.Clear();

            foreach (IEventListener listener in m_addingQueue)
            {
                m_listeners.AddLast(listener);
            }
            m_addingQueue.Clear();
        }
    }
}
