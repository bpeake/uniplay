﻿using System;
using System.Collections.Generic;
using UnityEngine.Assertions;

namespace UniPlay.Events
{
    public abstract class GlobalEventDispatcher : EventDispatcher
    {
        //A table of all dispatchers sorted by there type.
        private static Dictionary<Type, GlobalEventDispatcher> s_dispatchers = new Dictionary<Type, GlobalEventDispatcher>();

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static T GetDispatcher<T>() where T : GlobalEventDispatcher, new()
        {
            Type tType = typeof(T);
            GlobalEventDispatcher dispatcher;

            s_dispatchers.TryGetValue(tType, out dispatcher);
            T castedDispatcher = dispatcher as T;

            //This condition should never happen, if it does our table has been corrupted
            Assert.IsFalse(castedDispatcher == null && dispatcher != null);

            if (castedDispatcher == null)
            {
                castedDispatcher = new T();
                s_dispatchers.Add(tType, castedDispatcher);
            }

            return castedDispatcher;
        }

        protected GlobalEventDispatcher() : base()
        {
        }
    }
}
