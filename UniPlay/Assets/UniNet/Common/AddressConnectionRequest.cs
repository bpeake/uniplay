﻿
using System;

public class AddressConnectionRequest
    : IConnectionRequest
{
    private Uri m_address;

    public Uri Address
    {
        get { return m_address; }
    }

    public AddressConnectionRequest(Uri address)
    {
        m_address = address;
    }
}