﻿
using System.IO;

/// <summary>
/// A buffer that holds network formatted info.
/// </summary>
public class NetworkBuffer
{
    private NetworkStream m_baseStream;
    private Stream m_stream;

    public Stream BufferStream
    {
        get { return m_stream; }
    }

    internal NetworkStream BaseStream
    {
        get { return m_baseStream; }
    }

    public NetworkBuffer(int resizeLimit)
    {
        m_baseStream = new NetworkStream(resizeLimit);
        Startup();
    }

    public NetworkBuffer(byte[] rawBytes)
    {
        m_baseStream = new NetworkStream(rawBytes);
        Startup();
    }

    public NetworkBuffer(byte[] rawBytes, int offset, int count)
    {
        m_baseStream = new NetworkStream(rawBytes, offset, count);
        Startup();
    }

    private void Startup()
    {
        m_stream = CreateStream(m_baseStream);
    }

    protected virtual Stream CreateStream(NetworkStream baseStream)
    {
        return baseStream;
    }

    public void RegisterSizeEvent(NetworkStream.SizeLimitReachedDelegate callback)
    {
        m_baseStream.SizeLimitReached += callback;
    }

    public void UnregisterSizeEvent(NetworkStream.SizeLimitReachedDelegate callback)
    {
        m_baseStream.SizeLimitReached -= callback;
    }
}