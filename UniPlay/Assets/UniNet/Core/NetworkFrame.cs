﻿using UnityEngine;
using System.Collections;

public class NetworkFrame
{
    private byte[] buffer;

    public byte[] Buffer
    {
        get { return buffer; }
    }

    public NetworkFrame(int size)
    {
        buffer = new byte[size];
    }

    public NetworkFrame(byte[] src, int offset, int size)
    {
        buffer = new byte[size];
        src.CopyTo(buffer, offset);
    }

    public void Write(byte[] src, int offset, int size)
    {
        
    }
}
