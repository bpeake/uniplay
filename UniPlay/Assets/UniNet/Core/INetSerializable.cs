﻿
using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

public interface INetSerializable
{
    void Serialize(NetworkWriter writer);
    void Deserialize(NetworkReader reader);
}

public interface ITypeSerializer
{
    void Serialize(INetSerializable value, NetworkWriter writer);
    INetSerializable Deserialize(NetworkReader reader);
    INetSerializable CreateDefault();
}

public static class NetSerializableTypeStore
{
    private static Dictionary<uint, Type> s_serializableTypeLookup = new Dictionary<uint, Type>();
    private static Dictionary<Type, ITypeSerializer> s_typeSerializerLookup = new Dictionary<Type, ITypeSerializer>();

    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    private static void Startup()
    {
        Assembly[] assemblies = AppDomain.CurrentDomain.GetAssemblies();
        foreach (Assembly assembly in assemblies)
        {
            foreach (Type type in assembly.GetTypes())
            {
                if (typeof (INetSerializable).IsAssignableFrom(type))
                {
                    s_serializableTypeLookup.Add((uint) type.GetHashCode(), type);
                }
            }
        }
    }

    public static void AssignSerializer(ITypeSerializer serializer, Type type)
    {
        s_typeSerializerLookup.Add(type, serializer);
    }

    public static void RemoveSerializer(Type type)
    {
        s_typeSerializerLookup.Remove(type);
    }

    public static void ClearTypeSerializers()
    {
        s_typeSerializerLookup.Clear();
    }

    public static Type GetType(uint typeHash)
    {
        Type t;
        if (s_serializableTypeLookup.TryGetValue(typeHash, out t))
        {
            return t;
        }

        return null;
    }

    public static ITypeSerializer GetTypeSerializer(Type type)
    {
        ITypeSerializer serializer;
        if (s_typeSerializerLookup.TryGetValue(type, out serializer))
        {
            return serializer;
        }
        else if(!type.IsInterface)
        {
            Type[] interfaces = type.GetInterfaces();

            foreach (Type interfaceType in interfaces)
            {
                serializer = GetTypeSerializer(interfaceType);
                if(serializer != null)
                {
                    return serializer;
                }
            }

            return type.BaseType == null ? null : GetTypeSerializer(type.BaseType);
        }

        return null;
    }
}