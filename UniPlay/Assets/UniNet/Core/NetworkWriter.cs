﻿
using System.Collections;
using System.IO;
using System.IO.Compression;
using UnityEngine;

public sealed class NetworkWriter
    : BinaryWriter
{
    public NetworkWriter(NetworkBuffer buffer)
        : base(buffer.BufferStream)
    {
    }

    public void Write(Vector3 value)
    {
        Write(value.x);
        Write(value.y);
        Write(value.z);
    }

    public void Write(Vector2 value)
    {
        Write(value.x);
        Write(value.y);
    }

    public void Write(Quaternion value)
    {
        Write(value.eulerAngles);
    }

    public void Write(bool[] values)
    {
        BitArray bitArray = new BitArray(values);

        int byteCount = Mathf.CeilToInt(bitArray.Count / 8f);
        byte[] bytes = new byte[byteCount];

        bitArray.CopyTo(bytes, 0);

        Write((short)values.Length);

        for (int i = 0; i < bytes.Length; i++)
        {
            Write(bytes[i]);
        }
    }

    public void Write(INetSerializable value)
    {
        value.Serialize(this);
    }

    public void WriteWithType(INetSerializable value)
    {
        uint typeId = (uint) value.GetType().GetHashCode();

        Write(typeId);
        Write(value);
    }

    public void WriteWithSerializer(INetSerializable value)
    {
        ITypeSerializer serializer = NetSerializableTypeStore.GetTypeSerializer(value.GetType());

        serializer.Serialize(value, this);
    }
}