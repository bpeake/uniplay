﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

/// <summary>
/// A stream of network data.
/// </summary>
public sealed class NetworkStream : MemoryStream
{
    public delegate bool SizeLimitReachedDelegate(NetworkStream stream);

    private readonly int m_resizeLimit;

    public event SizeLimitReachedDelegate SizeLimitReached;

    /// <summary>
    /// The point at which other systems are informed about how many bytes have been written.
    /// </summary>
    public int ResizeLimit
    {
        get { return m_resizeLimit; }
    }

    public NetworkStream(int resizeLimit)
    {
        m_resizeLimit = resizeLimit;
    }

    public NetworkStream(byte[] buffer, int offset, int size)
    {
        m_resizeLimit = size;

        Write(buffer, offset, size);
    }

    public NetworkStream(byte[] buffer)
        : this(buffer, 0, buffer.Length)
    {
        
    }

    private bool OnSizeLimitReached()
    {
        var handler = SizeLimitReached;
        if (handler != null)
            return handler(this);

        return true;
    }

    public void ReadIn(byte[] buffer, int offset, int count)
    {
        Seek(0, SeekOrigin.Begin);
        SetLength(0);

        Write(buffer, offset, count);

        Seek(0, SeekOrigin.Begin);
    }

    public override void Write(byte[] buffer, int offset, int count)
    {
        int length = (int) Length;
        if ((length % m_resizeLimit) < (length + count % m_resizeLimit))
        {
            if (!OnSizeLimitReached())
            {
                throw new OutOfMemoryException("Memory stream is out of memory.");
            }
        }

        base.Write(buffer, offset, count);
    }

    public override void WriteByte(byte value)
    {
        int length = (int)Length;
        if ((length % m_resizeLimit) < (length + 1 % m_resizeLimit))
        {
            if (!OnSizeLimitReached())
            {
                throw new OutOfMemoryException("Memory stream is out of memory.");
            }
        }

        base.WriteByte(value);
    }
}
