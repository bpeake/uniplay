﻿
using System;
using System.Collections;
using System.IO;
using UnityEngine;
using UnityEngine.Assertions;

public sealed class NetworkReader
    : BinaryReader
{
    private NetworkBuffer m_buffer;

    public bool IsEof
    {
        get { return m_buffer.BaseStream.Position == m_buffer.BaseStream.Length; }
    }

    public NetworkReader(NetworkBuffer buffer)
        : base(buffer.BufferStream)
    {
        m_buffer = buffer;
    }

    public Vector3 ReadVector3()
    {
        Vector3 v;
        v.x = ReadSingle();
        v.y = ReadSingle();
        v.z = ReadSingle();

        return v;
    }

    public Vector2 ReadVector2()
    {
        Vector2 v;
        v.x = ReadSingle();
        v.y = ReadSingle();

        return v;
    }

    public Quaternion ReadQuaternion()
    {
        Quaternion q = new Quaternion {eulerAngles = ReadVector3()};

        return q;
    }

    public bool[] ReadBitArray()
    {
        short bitCount = ReadInt16();
        int byteCount = Mathf.CeilToInt(bitCount / 8f);

        byte[] bytes = ReadBytes(byteCount);

        BitArray bitArray = new BitArray(bytes);
        bool[] bools = new bool[bitCount];
        bitArray.CopyTo(bools, 0);

        return bools;
    }

    public T ReadSerializable<T>()
        where T : INetSerializable, new()
    {
        T newValue = new T();
        newValue.Deserialize(this);

        return newValue;
    }

    public void ReadSerializable<T>(T existing)
        where T : INetSerializable
    {
        existing.Deserialize(this);
    }

    public T ReadSerializableWithSerializer<T>()
        where T : INetSerializable
    {
        ITypeSerializer serializer = NetSerializableTypeStore.GetTypeSerializer(typeof (T));
        Assert.IsNotNull(serializer, "No serializer has been provided for this type, please provide one and then re-compile.");

        INetSerializable result = serializer.Deserialize(this);

        if (result == null)
        {
            return (T) serializer.CreateDefault();
        }
        else
        {
            Assert.IsTrue(result is T);
            return (T) result;
        }
    }

    public INetSerializable ReadSerializableWithTypeInfo()
    {
        Type type = NetSerializableTypeStore.GetType(ReadUInt32());
        Assert.IsNotNull(type);

        INetSerializable value = Activator.CreateInstance(type) as INetSerializable;
        Assert.IsNotNull(value);

        ReadSerializable(value);

        return value;
    }
}