﻿
using System.Collections.Generic;

public interface IChannelFactory
{
    IEnumerable<NetChannel> CreateChannel(NetConnection connection);
}