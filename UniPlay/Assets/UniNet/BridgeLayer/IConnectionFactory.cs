﻿
public interface IConnectionFactory
{
    NetConnection CreateConnection(NetBridge bridge);
}