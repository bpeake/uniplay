﻿
using System;
using System.Collections.Generic;
using System.Linq;

/// <summary>
/// Represents a container for connections and provides a factory for creating connections.
/// </summary>
public sealed class NetConnectionsSubsystem
{
    private LinkedList<NetConnection> m_connections = new LinkedList<NetConnection>();
    private IConnectionFactory m_factory;
    private NetBridge m_bridge;

    public NetConnectionsSubsystem(IConnectionFactory factory, NetBridge bridge)
    {
        m_factory = factory;
        m_bridge = bridge;
    }

    private NetConnection CreateConnectionObject()
    {
        return m_factory.CreateConnection(m_bridge);
    }

    /// <summary>
    /// Allocates a new connection object into the connection subsystem.
    /// </summary>
    /// <returns>The newly created connection object.</returns>
    public NetConnection AllocConnectionObject()
    {
        NetConnection newConnection = CreateConnectionObject();

        if (newConnection != null)
        {
            m_connections.AddLast(newConnection);
        }

        return newConnection;
    }

    /// <summary>
    /// Removes a connection after it is not longer needed.
    /// </summary>
    /// <param name="connection">The connection to remove.</param>
    public void RemoveConnection(NetConnection connection)
    {
        connection.Bridge.MarkClosed(connection);
        m_connections.Remove(connection);
    }

    /// <summary>
    /// Retrieves all useable connections in the subsystem.
    /// </summary>
    /// <returns>An enumerable of all useable connections in the subsystem.</returns>
    public IEnumerable<NetConnection> IterateConnections()
    {
        return m_connections.Where(connection => connection.IsOpen);
    }
}