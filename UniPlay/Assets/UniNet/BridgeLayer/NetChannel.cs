﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine.Assertions;
using UnityEngine.Networking;

public class ChannelTypeKey<T>
    : NetChannel.IKey
    where T : NetChannel
{
    public int Id { get { return typeof (T).GetHashCode(); } }
}

public class ChannelTypeKey
    : NetChannel.IKey
{
    private Type m_channelType;

    public ChannelTypeKey(Type channelType)
    {
        m_channelType = channelType;
    }

    public int Id { get { return m_channelType.GetHashCode(); } }
}

/// <summary>
/// A channel through which info can be sent.
/// </summary>
public abstract class NetChannel
{
    public interface IKey
    {
        int Id { get; }
    }

    public delegate void OnFlushDelegate(NetChannel source, byte[] data, int size);

    public delegate void OnReceiveDelegate(NetChannel source);

    private NetConnection m_connection;
    private NetworkBuffer m_sendingBuffer, m_receivingBuffer;
    private NetworkWriter m_sendingWriter;
    private NetworkReader m_receivingReader;

    public event OnFlushDelegate FlushEvent;
    public event OnReceiveDelegate ReceiveEvent;

    protected virtual int SendingBufferResizeLimit
    {
        get { return 1024; }
    }

    protected virtual int RecievingBufferResizeLimit
    {
        get { return ushort.MaxValue; }
    }

    public NetworkWriter SendingWriter
    {
        get { return m_sendingWriter; }
    }

    public NetworkReader ReceivingReader
    {
        get { return m_receivingReader; }
    }

    public NetConnection Connection
    {
        get { return m_connection; }
    }

    protected NetChannel(NetConnection connection)
    {
        m_connection = connection;
        SetupBuffers();
    }

    private void SetupBuffers()
    {
        m_receivingBuffer = new NetworkBuffer(RecievingBufferResizeLimit);
        m_sendingBuffer = new NetworkBuffer(SendingBufferResizeLimit);

        m_receivingBuffer.RegisterSizeEvent(OnRecievingBufferSizeLimitHit);
        m_sendingBuffer.RegisterSizeEvent(OnSendingBufferSizeLimitHit);

        m_sendingWriter = new NetworkWriter(m_sendingBuffer);
        m_receivingReader = new NetworkReader(m_receivingBuffer);
    }

    protected void OnFlushEvent(NetChannel source, byte[] data, int size)
    {
        var handler = FlushEvent;
        if (handler != null) handler(source, data, size);
    }

    protected void OnReceiveEvent(NetChannel source)
    {
        var handler = ReceiveEvent;
        if (handler != null) handler(source);
    }

    /// <summary>
    /// Called when the sending buffer hits a size limit.
    /// </summary>
    /// <param name="stream">The network stream that is being written to.</param>
    /// <returns>True if the write should still be done.</returns>
    protected virtual bool OnSendingBufferSizeLimitHit(NetworkStream stream)
    {
        return true;
    }

    /// <summary>
    /// Called when the recieving buffer hits a size limit.
    /// </summary>
    /// <param name="stream">The network stream that is being written to.</param>
    /// <returns>True if the write should still be done.</returns>
    protected virtual bool OnRecievingBufferSizeLimitHit(NetworkStream stream)
    {
        return true;
    }

    protected virtual void OnFlush()
    {
        
    }

    /// <summary>
    /// Called when data is recieved on this channel.
    /// </summary>
    /// <param name="data">The data that was recieved for this channel.</param>
    /// <param name="offset">The byte offset in the data to start from.</param>
    /// <param name="length">The length of the data.</param>
    public void Recieve(byte[] data, int offset, int length)
    {
        Assert.IsTrue(m_connection.IsOpen);

        m_receivingBuffer.BaseStream.ReadIn(data, offset, length);

        OnReceiveEvent(this);
    }

    /// <summary>
    /// Flushes the sending data on this channel.
    /// </summary>
    public void Flush()
    {
        NetworkStream stream = m_sendingBuffer.BaseStream;
        int size = (int) stream.Length;

        byte[] buffer = stream.GetBuffer();

        OnFlushEvent(this, buffer, size);
        OnFlush();

        stream.Seek(0, SeekOrigin.Begin);
        stream.SetLength(0);
    }

    /// <summary>
    /// Will flush the channel if the channel is marked as dirty.
    /// </summary>
    public void TryFlush()
    {
        if(IsDirty())
            Flush();
    }

    /// <summary>
    /// Checks to see if this net channel needs to flush.
    /// </summary>
    /// <returns>True if this channel needs to flush.</returns>
    public abstract bool IsDirty();

    /// <summary>
    /// Gets the key used to identify this network channel.
    /// </summary>
    /// <returns>The key used to identify this channel.</returns>
    public virtual IKey GetKey()
    {
        return new ChannelTypeKey(GetType());
    }
}
