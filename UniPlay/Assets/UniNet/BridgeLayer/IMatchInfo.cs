﻿
public interface IMatchInfo
{
    IConnectionRequest GenerateConnectionRequest();
}