﻿
using System.Collections.Generic;
using System.Linq;
using UnityEngine.Assertions;

/// <summary>
/// A bridge between the UniNet system and the lower level architecture.
/// </summary>
public class NetBridge
{
    public delegate void OnConnectionOpenDelegate(NetConnection connection);

    public delegate void OnConnectionCloseDelegate(NetConnection connection);

    private NetConnectionsSubsystem m_connectionsSubsystem;
    private LinkedList<NetworkArchitecture> m_architectures;

    public event OnConnectionOpenDelegate ConnectionOpenEvent;
    public event OnConnectionCloseDelegate ConnectionCloseEvent;

    /// <summary>
    /// The connection subsystem for this bridge.
    /// </summary>
    public NetConnectionsSubsystem ConnectionsSubsystem
    {
        get { return m_connectionsSubsystem; }
    }

    protected NetBridge(IConnectionFactory connectionFactory, IArchitecturesFactory architecturesFactory)
    {
        m_connectionsSubsystem = new NetConnectionsSubsystem(connectionFactory, this);
        m_architectures = new LinkedList<NetworkArchitecture>(architecturesFactory.CreateArchitectures(this));
    }

    protected virtual void OnConnectionOpenEvent(NetConnection connection)
    {
        var handler = ConnectionOpenEvent;
        if (handler != null) handler(connection);
    }

    protected virtual void OnConnectionCloseEvent(NetConnection connection)
    {
        var handler = ConnectionCloseEvent;
        if (handler != null) handler(connection);
    }

    /// <summary>
    /// Iterates over all architectures registered to this bridge.
    /// </summary>
    /// <returns>An enumerable of all architectures registered to this brige.</returns>
    public IEnumerable<NetworkArchitecture> IterateArchitectures()
    {
        return m_architectures;
    }

    /// <summary>
    /// Try to connect using a connection request.
    /// </summary>
    /// <param name="request">The request for a connection.</param>
    /// <returns>True if the connection request was handled.</returns>
    /// <remarks>Returning true does not mean a connection was made, only that there was some service to handle the request.</remarks>
    public bool TryToConnect(IConnectionRequest request)
    {
        return IterateArchitectures().Any(architecture => architecture.TryToConnect(request));
    }

    /// <summary>
    /// Try to join a match using match info.
    /// </summary>
    /// <param name="match">The match info.</param>
    /// <returns>True if the join request was handled.</returns>
    public bool TryToJoin(IMatchInfo match)
    {
        IConnectionRequest request = match.GenerateConnectionRequest();

        return TryToConnect(request);
    }

    /// <summary>
    /// Marks a connection as open.
    /// </summary>
    /// <param name="connection">The connection to open.</param>
    public void MarkOpen(NetConnection connection)
    {
        if (!connection.IsOpen)
        {
            connection.OnOpen();
            OnConnectionOpenEvent(connection);
        }
    }

    /// <summary>
    /// Marks a connection as closed.
    /// </summary>
    /// <param name="connection">The connection to close.</param>
    public void MarkClosed(NetConnection connection)
    {
        if (connection.IsOpen)
        {
            connection.OnClose();
            OnConnectionCloseEvent(connection);
        }
    }

    /// <summary>
    /// Processes one tick of the bridge.
    /// </summary>
    public virtual void Process()
    {
        foreach (NetConnection connection in ConnectionsSubsystem.IterateConnections())
        {
            connection.TryFlush();
        }

        foreach (NetworkArchitecture architecture in IterateArchitectures())
        {
            architecture.Process();
        }
    }
}