﻿
using System.Collections.Generic;

public interface IArchitecturesFactory
{
    IEnumerable<NetworkArchitecture> CreateArchitectures(NetBridge bridge);
}