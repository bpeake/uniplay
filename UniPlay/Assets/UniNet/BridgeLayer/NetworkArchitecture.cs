﻿
using System;
using System.Collections.Generic;
using System.Linq;

/// <summary>
/// Represents a network architecture.
/// </summary>
public abstract class NetworkArchitecture
{
    /// <summary>
    /// A handler for connection requests.
    /// </summary>
    public abstract class ConnectionRequestResolver
    {
        private NetworkArchitecture m_architecture;

        public NetworkArchitecture Architecture
        {
            get { return m_architecture; }
        }

        protected ConnectionRequestResolver(NetworkArchitecture architecture)
        {
            m_architecture = architecture;
        }

        public abstract bool TryToConnect(IConnectionRequest request);
    }

    private NetBridge m_owner;
    private LinkedList<ConnectionRequestResolver> m_resolvers;

    public NetBridge Owner
    {
        get { return m_owner; }
    }

    protected NetworkArchitecture(NetBridge owner)
    {
        m_owner = owner;
        Setup();
    }

    private void Setup()
    {
        m_resolvers = new LinkedList<ConnectionRequestResolver>(CreateResolvers());
    }

    /// <summary>
    /// Creates the connection resolvers to use for this architecure.
    /// </summary>
    /// <returns>True</returns>
    protected abstract ConnectionRequestResolver[] CreateResolvers();

    /// <summary>
    /// Called when this architecure is told to process.
    /// </summary>
    protected abstract void OnProcess();

    protected abstract void OnConnectionOpen(NetConnection connection);

    protected abstract void OnConnectionClose(NetConnection connection);

    /// <summary>
    /// Called when a channel flushes its data.
    /// </summary>
    /// <param name="source">The source channel that is flushing.</param>
    /// <param name="data">The data that is flushing.</param>
    /// <param name="size">The size of the data that is flushing.</param>
    protected abstract void OnChannelFlush(NetChannel source, byte[] data, int size);

    /// <summary>
    /// Allocate a connection object from the bridge.
    /// </summary>
    /// <returns>The new closed connection allocated from the bridge.</returns>
    protected NetConnection AllocConnectionFromBridge()
    {
        NetConnection connection = m_owner.ConnectionsSubsystem.AllocConnectionObject();
        connection.OpenEvent += OnConnectionOpen;
        connection.CloseEvent += OnConnectionClose;

        foreach (NetChannel channel in connection.IterateChannels<NetChannel>())
        {
            channel.FlushEvent += OnChannelFlush;
        }

        return connection;
    }

    protected void RemoveConnectionFromBridge(NetConnection connection)
    {
        m_owner.ConnectionsSubsystem.RemoveConnection(connection);
    }

    /// <summary>
    /// Trys to make a connection to a destination.
    /// </summary>
    /// <param name="request">The request for connection.</param>
    /// <returns>True if a connection could be made, false if unhandled or on failure.</returns>
    public bool TryToConnect(IConnectionRequest request)
    {
        return m_resolvers.Any(resolver => resolver.TryToConnect(request));
    }

    public void Process()
    {
        OnProcess();
    }
}