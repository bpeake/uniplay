﻿
using System.Collections.Generic;
using System.Linq;
using UniPlay.Events;

public interface IConnectionEventListener 
    : IEventListener<ConnectionEventDispatcher>
{
    void OnConnectionOpenEvent(NetConnection connection);
    void OnConnectionCloseEvent(NetConnection connection);
}

public class ConnectionEventDispatcher 
    : GlobalEventDispatcher
{
    public void OnConnectionOpen(NetConnection connection)
    {
        foreach (IConnectionEventListener listener in IterateListeners<IConnectionEventListener>())
        {
            listener.OnConnectionOpenEvent(connection);
        }
    }

    public void OnConnectionClose(NetConnection connection)
    {
        foreach (IConnectionEventListener listener in IterateListeners<IConnectionEventListener>())
        {
            listener.OnConnectionCloseEvent(connection);
        }
    }
}

/// <summary>
/// A representation of a connection.
/// </summary>
public abstract class NetConnection
{
    public delegate void OnConnectionOpenDelegate(NetConnection connection);

    public delegate void OnConnectionCloseDelegate(NetConnection connection);

    private Dictionary<int, NetChannel> m_channelsLookup = new Dictionary<int, NetChannel>();
    private bool m_isOpen;
    private NetBridge m_bridge;

    public event OnConnectionOpenDelegate OpenEvent;
    public event OnConnectionCloseDelegate CloseEvent;

    public bool IsOpen
    {
        get { return m_isOpen; }
    }

    public NetBridge Bridge
    {
        get { return m_bridge; }
    }

    protected NetConnection(NetBridge bridge)
    {
        m_bridge = bridge;
        Setup();
    }

    private void Setup()
    {
        foreach (NetChannel channel in CreateChannels())
        {
            m_channelsLookup.Add(channel.GetKey().Id, channel);
        }
    }

    private void OnOpenEvent()
    {
        var handler = OpenEvent;
        if (handler != null) handler(this);
    }

    private void OnCloseEvent()
    {
        var handler = CloseEvent;
        if (handler != null) handler(this);
    }

    protected abstract IEnumerable<NetChannel> CreateChannels();

    /// <summary>
    /// Gets the channel at a specific index.
    /// </summary>
    /// <typeparam name="T">The type of net channel to return as.</typeparam>
    /// <param name="channelKey">The key of the channel to get.</param>
    /// <returns>The net channel at the specific index.</returns>
    public T GetChannel<T>(NetChannel.IKey channelKey)
        where T : NetChannel
    {
        return m_channelsLookup[channelKey.Id] as T;
    }

    /// <summary>
    /// Get the channel with a specific id for its key.
    /// </summary>
    /// <typeparam name="T">The type of channel to try and get.</typeparam>
    /// <param name="channelId">The id of the channel.</param>
    /// <returns>The channel with that id.</returns>
    public T GetChannel<T>(int channelId)
        where T : NetChannel
    {
        return m_channelsLookup[channelId] as T;
    }

    /// <summary>
    /// Iterates all channels of a type.
    /// </summary>
    /// <typeparam name="T">The type of channel to iterate.</typeparam>
    /// <returns>An iterator for all channels of type T.</returns>
    public IEnumerable<T> IterateChannels<T>()
        where T : NetChannel
    {
        return m_channelsLookup.Values.OfType<T>();
    }

    /// <summary>
    /// Forces all channels to flush there data.
    /// </summary>
    public void ForceFlush()
    {
        foreach (NetChannel channel in m_channelsLookup.Values)
        {
            channel.Flush();
        }
    }

    /// <summary>
    /// Flushes all channels marked as dirty.
    /// </summary>
    public void TryFlush()
    {
        foreach (NetChannel channel in m_channelsLookup.Values)
        {
            channel.TryFlush();
        }
    }

    public virtual void OnOpen()
    {
        m_isOpen = true;
        OnOpenEvent();
        GlobalEventDispatcher.GetDispatcher<ConnectionEventDispatcher>().OnConnectionOpen(this);
    }

    public virtual void OnClose()
    {
        m_isOpen = false;
        OnCloseEvent();
        GlobalEventDispatcher.GetDispatcher<ConnectionEventDispatcher>().OnConnectionClose(this);
    }

    public void Open()
    {
        m_bridge.MarkOpen(this);
    }

    public void Close()
    {
        m_bridge.MarkClosed(this);
    }
}