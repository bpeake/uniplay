﻿
using UnityEngine.Networking;

public sealed class UNetAddressConnectionRequestResover
    : UNetLowLevel.UNetConnectionRequestResolver
{
    public UNetAddressConnectionRequestResover(NetworkArchitecture architecture) 
        : base(architecture)
    {
    }

    public override bool TryToConnect(IConnectionRequest request)
    {
        AddressConnectionRequest addressRequest = request as AddressConnectionRequest;
        if (addressRequest == null)
        {
            return false;
        }

        int hostId;
        UNetAddressConnectionRequest uNetAddressConnectionRequest = addressRequest as UNetAddressConnectionRequest;
        hostId = uNetAddressConnectionRequest != null ? uNetAddressConnectionRequest.HostId : DefaultHost;

        byte error;
        NetworkTransport.Connect(hostId, addressRequest.Address.Host, addressRequest.Address.Port, -1, out error);

        if (error != (int) NetworkError.Ok)
        {
            return false;
        }

        return true;
    }
}