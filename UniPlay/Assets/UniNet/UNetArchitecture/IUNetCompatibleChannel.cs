﻿
using UnityEngine.Networking;

public interface IUNetCompatibleChannel
{
    QosType ChannelSendMode { get; }
}