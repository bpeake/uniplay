﻿
using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Networking;

public class UNetLowLevel
    : NetworkArchitecture
{
    public class HostInfo
    {
        private Dictionary<int, NetConnection> m_connectionMap = new Dictionary<int, NetConnection>(); 
        private Dictionary<NetConnection, int> m_connectionIdMap = new Dictionary<NetConnection, int>();

        public int Id { get; set; }

        public NetConnection GetConnection(int connectionId)
        {
            Assert.IsTrue(m_connectionMap.ContainsKey(connectionId));

            return m_connectionMap[connectionId];
        }

        public int GetConnectionId(NetConnection connection)
        {
            Assert.IsTrue(m_connectionIdMap.ContainsKey(connection));

            return m_connectionIdMap[connection];
        }

        public void AddConnection(int connectionId, NetConnection connection)
        {
            Assert.IsFalse(m_connectionMap.ContainsKey(connectionId));
            Assert.IsFalse(m_connectionIdMap.ContainsKey(connection));

            m_connectionMap.Add(connectionId, connection);
            m_connectionIdMap.Add(connection, connectionId);
        }

        public NetConnection RemoveConnection(int connectionId)
        {
            Assert.IsTrue(m_connectionMap.ContainsKey(connectionId));

            NetConnection connection = GetConnection(connectionId);
            m_connectionMap.Remove(connectionId);

            Assert.IsTrue(m_connectionIdMap.ContainsKey(connection));

            m_connectionIdMap.Remove(connection);

            return connection;
        }

        public bool Close()
        {
            foreach (NetConnection connection in m_connectionMap.Values)
            {
                connection.Bridge.ConnectionsSubsystem.RemoveConnection(connection);
            }

            return NetworkTransport.RemoveHost(Id);
        }
    }

    public abstract class UNetConnectionRequestResolver
        : ConnectionRequestResolver
    {
        private UNetLowLevel m_unet;

        /// <summary>
        /// The default host id that unet is using.
        /// </summary>
        protected int DefaultHost
        {
            get { return m_unet.m_defaultHost; }
        }

        /// <summary>
        /// Dictionary of host info.
        /// </summary>
        protected Dictionary<int, HostInfo> HostLookup
        {
            get { return m_unet.m_hostInfoLookup; }
        } 

        protected UNetConnectionRequestResolver(NetworkArchitecture architecture) 
            : base(architecture)
        {
            m_unet = architecture as UNetLowLevel;
        }
    }

    private byte[] m_mainBuffer;
    private int m_mainBufferSize;
    private int m_defaultHost = -1;
    private HostTopology m_defaulTopology;

    private Dictionary<int, HostInfo> m_hostInfoLookup = new Dictionary<int, HostInfo>();
    private Dictionary<NetConnection, HostInfo> m_hostInfoLookupByConnection = new Dictionary<NetConnection, HostInfo>(); 

    public UNetLowLevel(NetBridge owner, HostTopology topology) 
        : base(owner)
    {
        m_mainBuffer = new byte[ushort.MaxValue];
        m_mainBufferSize = 0;
        m_defaulTopology = topology;
    }

    private void OnDataEvent(int hostId, int connectionId, int channelId)
    {
        HostInfo hostInfo = m_hostInfoLookup[hostId];
        NetConnection connection = hostInfo.GetConnection(connectionId);

        int internalChannelId;
        using (MemoryStream stream = new NetworkStream(m_mainBuffer, 0, sizeof (int)))
        {
            using (BinaryReader reader = new BinaryReader(stream))
            {
                internalChannelId = reader.ReadInt32();
            }
        }

        NetChannel channelBuffer = connection.GetChannel<NetChannel>(internalChannelId);
        channelBuffer.Recieve(m_mainBuffer, sizeof (int), m_mainBufferSize - sizeof (int));
    }

    private void OnConnectEvent(int hostId, int connectionId)
    {
        NetConnection connection = AllocConnectionFromBridge();
        HostInfo host = m_hostInfoLookup[hostId];

        host.AddConnection(connectionId, connection);

        m_hostInfoLookupByConnection.Add(connection, host);
    }

    private void OnDisconnectEvent(int hostId, int connectionId)
    {
        if (!m_hostInfoLookup.ContainsKey(hostId))
            return;

        HostInfo host = m_hostInfoLookup[hostId];
        NetConnection connection = host.GetConnection(connectionId);

        if(connection != null)
            connection.Bridge.MarkClosed(connection);
    }

    protected override ConnectionRequestResolver[] CreateResolvers()
    {
        return new ConnectionRequestResolver[]
        {
            new UNetAddressConnectionRequestResover(this), 
        };
    }

    protected override void OnConnectionClose(NetConnection connection)
    {
        HostInfo hostInfo = m_hostInfoLookupByConnection[connection];

        byte error;
        if (!NetworkTransport.Disconnect(hostInfo.Id, hostInfo.GetConnectionId(connection), out error))
        {
            Debug.LogWarning("The connection could not be closed. This might be because the internal architecture already closed the connection.");
        }

        hostInfo.RemoveConnection(hostInfo.GetConnectionId(connection));
    }

    protected override void OnConnectionOpen(NetConnection connection)
    {
    }

    protected override void OnChannelFlush(NetChannel source, byte[] data, int size)
    {
        if (m_mainBuffer.Length < size)
        {
            m_mainBuffer = new byte[size];
        }

        using (MemoryStream stream = new NetworkStream(m_mainBuffer))
        {
            using (BinaryWriter writer = new BinaryWriter(stream))
            {
                writer.Write(source.GetKey().Id);
            }

            stream.Write(data, 0, size);
            m_mainBufferSize = (int) stream.Length;
        }

        HostInfo host = m_hostInfoLookupByConnection[source.Connection];

        int channelId;
        IUNetCompatibleChannel uNetChannelInfo = source as IUNetCompatibleChannel;
        if (uNetChannelInfo != null)
        {
            channelId = (int) uNetChannelInfo.ChannelSendMode;
        }
        else
        {
            channelId = (int) QosType.Reliable;
        }

        byte error;
        NetworkTransport.Send(host.Id, host.GetConnectionId(source.Connection), channelId, m_mainBuffer,
            m_mainBufferSize, out error);
    }

    protected override void OnProcess()
    {
        if (m_hostInfoLookup.Count == 0)
        {
            if (!AddDefaultHost())
            {
                return;//Don't process with no host
            }
        }

        for (int i = 0; i < 100; i++)
        {
            int hostId, connectionId, channelId, recievedSize;
            byte error;

            NetworkEventType type = NetworkTransport.Receive(out hostId, out connectionId, out channelId, m_mainBuffer,
                m_mainBuffer.Length,
                out recievedSize, out error);

            NetworkError netError = (NetworkError) error;
            if (netError == NetworkError.MessageToLong && recievedSize > m_mainBuffer.Length)
            {
                m_mainBuffer = new byte[recievedSize];
                continue;
            }
            else if (netError != NetworkError.Ok)
            {
                Debug.LogErrorFormat("Error: Network error {0} on recieve.", netError);
                break;
            }

            switch (type)
            {
                case NetworkEventType.DataEvent:
                    m_mainBufferSize = recievedSize;
                    OnDataEvent(hostId, connectionId, channelId);
                    break;
                case NetworkEventType.ConnectEvent:
                    OnConnectEvent(hostId, connectionId);
                    break;
                case NetworkEventType.DisconnectEvent:
                    OnDisconnectEvent(hostId, connectionId);
                    break;
                case NetworkEventType.Nothing:
                    i = 100;
                    break;
                case NetworkEventType.BroadcastEvent:
                    //TODO: Add support for broadcast events
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }

    public bool AddDefaultHost()
    {
        if (m_defaultHost != -1)
        {
            if (!CloseHost(m_defaultHost))
            {
                return false;
            }
        }

        return AddHost(0) >= 0;
    }

    public int AddHost(HostTopology topology, string ip, int port)
    {
        List<ChannelQOS> defaultChannelTypes = new List<ChannelQOS>()
        {
            new ChannelQOS(QosType.Unreliable),
            new ChannelQOS(QosType.UnreliableFragmented),
            new ChannelQOS(QosType.UnreliableSequenced),
            new ChannelQOS(QosType.Reliable),
            new ChannelQOS(QosType.ReliableFragmented),
            new ChannelQOS(QosType.ReliableSequenced),
            new ChannelQOS(QosType.StateUpdate),
            new ChannelQOS(QosType.ReliableStateUpdate),
            new ChannelQOS(QosType.AllCostDelivery)
        };

        topology.DefaultConfig.Channels.Clear();
        topology.DefaultConfig.Channels.AddRange(defaultChannelTypes);

        foreach (ConnectionConfig connectionConfig in topology.SpecialConnectionConfigs)
        {
            connectionConfig.Channels.Clear();
            connectionConfig.Channels.AddRange(defaultChannelTypes);
        }

        int newHostId = NetworkTransport.AddHost(topology, port, ip);

        if (newHostId < 0)
        {
            return newHostId;
        }

        Assert.IsFalse(m_hostInfoLookup.ContainsKey(newHostId));

        m_hostInfoLookup.Add(newHostId, new HostInfo());

        return newHostId;
    }

    public int AddHost(string ip, int port)
    {
        return AddHost(m_defaulTopology, ip, port);
    }

    public int AddHost(int port)
    {
        return AddHost(m_defaulTopology, null, port);
    }

    public bool CloseHost(int hostId)
    {
        return m_hostInfoLookup.ContainsKey(hostId) && m_hostInfoLookup[hostId].Close();
    }
}