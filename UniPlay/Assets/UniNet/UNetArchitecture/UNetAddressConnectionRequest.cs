﻿
using System;

public class UNetAddressConnectionRequest
    : AddressConnectionRequest
{
    private int m_hostId;

    public int HostId
    {
        get { return m_hostId; }
    }

    public UNetAddressConnectionRequest(int hostId, Uri address) 
        : base(address)
    {
        m_hostId = hostId;
    }
}