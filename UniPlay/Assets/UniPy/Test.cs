﻿using UnityEngine;
using System.Collections;
using Microsoft.Scripting.Hosting;

public class Test : MonoBehaviour {

	// Use this for initialization
	void Start ()
	{
	    ScriptEngine se = IronPython.Hosting.Python.CreateEngine();
	    ScriptScope scope = se.Runtime.CreateScope();

	    ScriptSource source = se.CreateScriptSourceFromString(Resources.Load<TextAsset>("test").text);
	    source.Execute(scope);

	    Debug.Log(scope.GetVariable("value"));
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
